import React from "react";
import { Grid, IconButton } from "@mui/material";
import { useSelector } from "react-redux";
import commonService from "../../service/menu/commonService";
import Image from "next/image";
import Link from "next/link";
import DeleteIcon from "@mui/icons-material/Delete";

export default function WishList() {
  // hooks
  const userInfo = useSelector((state) => state.auth.userInfo);

  // state
  const [allWishList, setAllWishList] = React.useState([]);

  // methods
  const getWishList = () => {
    commonService
      .authGetData("wishList", userInfo.token)
      .then((res) => {
        setAllWishList(res.data.data);
      })
      .catch((err) => console.log(err));
  };

  const removeWishList = (id) => {
    commonService
      .authGetData("removeWishList/" + id , userInfo.token)
      .then((res) => {
        getWishList();
      })
      .catch((err) => console.log(err));
  }

  // sideeffects
  React.useEffect(() => {
    getWishList();
  }, []);

  return (
    <>

      <Grid container spacing={2}>
        <Grid container spacing={2}>
        <Grid item sm={12}>
          <h1>Your Wish List</h1>
        </Grid>
        </Grid>
      </Grid>
      <Grid container spacing={2}>
        {allWishList.map((wish, pos) => {
          return (
            <Grid item sm={3} key={pos}>
              <Link href={`/product/${wish.slug}`}>
                <a>
                  <Image
                    src={`https://images.miah.shop/product/m_thumb/${wish.img}`}
                    alt="Picture of the author"
                    width={300}
                    height={300}
                    layout="responsive"
                  />
                  {wish.name}
                </a>
              </Link>
              <div>
                <small>TK {wish.sales_cost}</small>
              </div>
              <div>
                <IconButton onClick={()=> removeWishList(wish.product_id)} aria-label="delete">
                  <DeleteIcon />
                </IconButton>
              </div>
            </Grid>
          );
        })}
      </Grid>
    </>
  );
}
