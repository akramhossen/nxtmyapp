import React from "react";
import Grid from "@mui/material/Grid";
import DashboradSideMenu from "./DashboradSideMenu";

export default function LayoutDashboard({children}) {
  return (
    <Grid container spacing={2} pt={5}>
      <Grid sm={4} xs={12} item>
        <DashboradSideMenu />
      </Grid>
      <Grid sm={8} xs={12} item>
        {children}
      </Grid>
    </Grid>
  );
}
