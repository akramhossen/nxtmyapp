import React, { useState, useEffect } from "react";
import { Grid, Card, Divider, TextField, Button } from "@mui/material";
import commonService from "../../service/menu/commonService";
import { useSelector } from "react-redux";

export default function RewardCashback() {
  // hooks
  const userInfo = useSelector((state) => state.auth.userInfo);

  // local state
  const [points, setPoints] = useState(0);
  const [msg, setMsg] = useState("");
  const [data, setData] = useState({
    credit_amount: 0,
    pending_credit_amount: 0,
    pending_reward: 0,
    reward_point: 0,
    status: 1,
  });

  // methods
  const handlePoints = (e) => {
    setPoints(e.target.value);
    console.log(e.target.value);
  };
  const convertPoints = () => {
    setMsg("");
    const body = {
      credit_point: points,
    };
    commonService
      .postAuthData("convertRewardPoint_toCredit", body, userInfo.token)
      .then((res) => {
        if (res.data.status === 1) {
          setPoints(null);
          setMsg(res.data.msg);
        }
        if (res.data.status === 0) {
          setMsg(res.data.msg);
        }
        console.log(res);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const getRewards = () => {
    commonService
      .authGetData("rewardPoint", userInfo.token)
      .then((res) => {
        setData(res.data)
        console.log(res);
      })
      .catch((err) => console.log(err));
  };

  useEffect(() => {
    getRewards();
  }, []);

  return (
    <>
      <h1>Reward Points and Cashback</h1>
      <Grid container spacing={2}>
        <Grid item sm={5} xs={12}>
          <Card>
            <Grid container spacing={2} p={2}>
              <Grid item sm={6}>
                <h3>
                  <img
                    width="30px"
                    src="https://miah.shop/img/credits.a128aae5.svg"
                    alt="coinlogo"
                  />{" "}
                  Credits
                </h3>
                <p>
                  <small>Pending Credits { data.pending_credit_amount }  Tk</small>
                </p>
              </Grid>
              <Grid item sm={6}>
                <h3>{ data.pending_credit_amount } Tk</h3>
              </Grid>
            </Grid>
          </Card>
        </Grid>
        <Grid item  sm={5} xs={12}>
          <Card>
            <Grid container spacing={2} p={2}>
              <Grid item sm={6}>
                <h3>
                  <img
                    width="30px"
                    src="https://miah.shop/img/credits.a128aae5.svg"
                    alt="coinlogo"
                  />{" "}
                  Points
                </h3>
              </Grid>
              <Grid item sm={6}>
                <h3>{ data.reward_point }  Tk</h3>
                <p>
                  <small>*Pending points { data.pending_reward } </small>
                </p>
              </Grid>
            </Grid>
          </Card>
        </Grid>
        <Grid item sm={12}>
          <Divider />
        </Grid>
        <Grid container spacing={2} p={2}>
          <Grid item sm={5} xs={12}>
            <Card sx={{ padding: "15px" }}>
              <h4>Convert Your Points To Credit</h4>
              <TextField
                required
                fullWidth
                onChange={handlePoints}
                variant="standard"
                label="Enter Points"
              />
              <Button
                variant="contained"
                sx={{ marginTop: "15px" }}
                size="small"
                onClick={convertPoints}
              >
                Convert
              </Button>
              <div>{msg}</div>
            </Card>
          </Grid>
        </Grid>
      </Grid>
    </>
  );
}
