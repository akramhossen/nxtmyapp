import React, { useEffect, useState } from "react";
import Breadcrumbs from "@mui/material/Breadcrumbs";
import { Typography, Link } from "@mui/material";
import HeadComponent from "../formUI/head/HeadComponent";

export default function MiahBreadCrumbs({ type, data }) {
  // local state
  const [breadCrumbs, setBreadCrumbs] = useState([]);
  const [title, setTitle] = useState("");

  // methods

  const createBreadCumbs = () => {
    setBreadCrumbs([]);
    if (type === "rootCategory") {
      // setBreadCrumbs([
      //   { title: data.breadCam.root_category, slug: data.breadCam.slug },
      // ]);
      setTitle(data.breadCam.root_category);
    } else if (type === "category") {
      const categoryBreads = [
        {
          title: data.breadCam.root_category,
          slug: `/${data.breadCam.department_slug}`,
        },
      ];
      setBreadCrumbs(categoryBreads);
      setTitle(data.breadCam.category);
    } else if (type === "subcategory") {
      const subCatBreads = [
        {
          title: data.breadCam.root_category,
          slug: `/${data.breadCam.department_slug}`,
        },
        {
          title: data.breadCam.category,
          slug: `/${data.breadCam.department_slug}/${data.breadCam.category_slug}`,
        },
      ];
      setBreadCrumbs(subCatBreads);
      setTitle(data.breadCam.sub_category);
    } else if (type === "product") {
      const podctBreads = [
        {
          title: data.breadCam.root_category,
          slug: `/${data.breadCam.department_slug}`,
        },
        {
          title: data.breadCam.category,
          slug: `/${data.breadCam.department_slug}/${data.breadCam.category_slug}`,
        },
        {
          title: data.breadCam.sub_category,
          slug: `/${data.breadCam.department_slug}/${data.breadCam.category_slug}/${data.breadCam.slug}`,
        },
      ];
      setBreadCrumbs(podctBreads);
      setTitle("");
    }
  };

  function handleClick(event) {
    event.preventDefault();
    console.info("You clicked a breadcrumb.");
  }

  // side effects
  useEffect(() => {
    createBreadCumbs();
  }, [data]);
  return (
    <>
      <HeadComponent data={data} />
      <Breadcrumbs separator="›" aria-label="breadcrumb">
        <Link underline="hover" key={11} color="inherit" href="/">
          Home
        </Link>
        {breadCrumbs.map((item, pos) => {
          return (
            <Link underline="hover" key={pos} color="inherit" href={item.slug}>
              {item.title}
            </Link>
          );
        })}
        {type === "product" ? null : (
          <Typography key="3" color="text.primary">
            {title}
          </Typography>
        )}
      </Breadcrumbs>
    </>
  );
}
