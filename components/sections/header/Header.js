import React, { useEffect } from "react";
import DesktopNav from "../../header/DesktopNav";
import Logo from "../../header/Logo";
import TopBarDesktop from "../../header/TopBarDesktop";
import DrawerMain from "../../header/drawer/DrawerMain";
// import CatDrawer from '../../header/drawer/CatDrawer'
import AppBar from "@mui/material/AppBar";
import useScrollTrigger from "@mui/material/useScrollTrigger";
import Slide from "@mui/material/Slide";
import Box from "@mui/material/Box";
import { useSelector, useDispatch } from "react-redux";
import { fetchMenu, fetchStieOptions } from "../../../redux/menu/menuActions";

function HideOnScroll(props) {
  const { children, window } = props;
  const trigger = useScrollTrigger({
    target: window ? window() : undefined,
  });

  return (
    <Slide appear={false} direction="down" in={!trigger}>
      {children}
    </Slide>
  );
}

export default function Header(props) {
  const menu = useSelector((state) => state.menu.menu);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(fetchMenu());
    dispatch(fetchStieOptions());
  }, [dispatch]);
  return (
    <>
      <HideOnScroll {...props}>
        <AppBar sx={{ boxShadow: "none" }}>
          <TopBarDesktop menutype="top" />
          <Box sx={{ display: { xs: "none", sm: "block" } }}>
            <DesktopNav menu={menu} />
          </Box>
          <DrawerMain menu={menu} />
        </AppBar>
      </HideOnScroll>
      <AppBar
        position="fixed"
        sx={{
          display: { xs: "block", sm: "none" },
          top: "auto",
          bottom: 0,
        }}
      >
        <TopBarDesktop />
        <Box sx={{ display: { xs: "none", sm: "block" } }}>
          <DesktopNav menu={menu} />
        </Box>
        <DrawerMain menu={menu} />
      </AppBar>
    </>
  );
}
