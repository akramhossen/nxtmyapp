import React from "react";
import Image from "next/image";
import useWindowSize from "../../../../customHooks/useWindowSize";
import { Box } from "@mui/material";
import Link from "next/link";
import { MiahHomeButton } from "../../../button/MiahButton";

export default function ImageContainer({ src, text, url, deviceWidth }) {
  const size = useWindowSize();
  return (
    <div style={{ position: "relative" }}>
      <Image
        src={src}
        alt="Miah Banner"
        width={size.width}
        height={size.height - 70}
        layout="responsive"
        objectFit="cover"
        priority={text === "Abaya" ? true : false}
      />
      {deviceWidth ? (
        <Box sx={{ textAlign: "center" }}>
          <p
            style={{
              color: "#000",
              fontSize: ".75rem",
              marginTop: "5px",
              marginBottom: "5px",
              textTransform: "uppercase",
            }}
          >
            <Link href={url}>
              <a>
                <u>Shop Now</u>
              </a>
            </Link>
          </p>
          <p
            style={{
              color: "#000",
              fontSize: "1.3rem",
              marginTop: "0px",
              marginBottom: "5px",
              textTransform: "uppercase",
            }}
          >
            <Link href={url}>
              <a>{text}</a>
            </Link>
          </p>
        </Box>
      ) : (
        <Box
          sx={{
            position: "absolute",
            bottom: "10%",
            right: "10%",
            textAlign: "center",
          }}
        >
          <p
            style={{
              color: "#fff",
              fontSize: "1.8rem",
              marginBottom: "5px",
              textTransform: "uppercase",
            }}
          >
            {text}
          </p>
          <Link href={url}>
            <a>
              <MiahHomeButton variant="outlined" size="large">
                Shop Now
              </MiahHomeButton>
            </a>
          </Link>
        </Box>
      )}
    </div>
  );
}
