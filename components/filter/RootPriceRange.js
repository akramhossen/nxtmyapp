import * as React from "react";
import { styled } from "@mui/material/styles";
import ArrowForwardIosSharpIcon from "@mui/icons-material/ArrowForwardIosSharp";
import MuiAccordion from "@mui/material/Accordion";
import MuiAccordionSummary from "@mui/material/AccordionSummary";
import MuiAccordionDetails from "@mui/material/AccordionDetails";
import { Typography, Slider, Grid } from "@mui/material";
import NProgress from "nprogress";
import axios from "axios";
import { useRouter } from "next/router";
import { BASE_URL } from "../../service/serviceConfig";

function valuetext(value) {
  return `${value}°C`;
}

const Accordion = styled((props) => (
  <MuiAccordion disableGutters elevation={0} square {...props} />
))(({ theme }) => ({
  border: `1px solid ${theme.palette.divider}`,
  "&:not(:last-child)": {
    borderBottom: 0,
  },
  "&:before": {
    display: "none",
  },
}));

const AccordionSummary = styled((props) => (
  <MuiAccordionSummary
    expandIcon={<ArrowForwardIosSharpIcon sx={{ fontSize: "0.9rem" }} />}
    {...props}
  />
))(({ theme }) => ({
  backgroundColor:
    theme.palette.mode === "dark"
      ? "rgba(255, 255, 255, .05)"
      : "rgba(0, 0, 0, .03)",
  flexDirection: "row-reverse",
  "& .MuiAccordionSummary-expandIconWrapper.Mui-expanded": {
    transform: "rotate(90deg)",
  },
  "& .MuiAccordionSummary-content": {
    marginLeft: theme.spacing(1),
  },
}));

const AccordionDetails = styled(MuiAccordionDetails)(({ theme }) => ({
  padding: theme.spacing(2),
  borderTop: "1px solid rgba(0, 0, 0, .125)",
}));

export default function RootPriceRange({
  products,
  params
}) {
  // ==================== hooks ====================
  const fetcher = (url) => axios.get(url).then((res) => res.data);
  const router = useRouter();

  // =================state=========================
  const [expanded, setExpanded] = React.useState("panel1");
  const [value, setValue] = React.useState([1, parseInt(products.maxRate)]);
  const [maxValue, setMaxValue] = React.useState(parseInt(products.maxRate));
  const [minValue, setMinValue] = React.useState(parseInt(1));
  const [comitedValue, setComittedValue] = React.useState([
    0,
    parseInt(products.maxRate),
  ]);

  // ====================methods==============
  const handleSlider = (event, newValue) => {
    setValue(newValue);
  };
  const handleChange = (panel) => (event, newExpanded) => {
    setExpanded(newExpanded ? panel : false);
  };

  const handleSliderCommitted = async (event, newValue) => {
    setComittedValue(newValue);
    if (router.asPath.indexOf("from=") !== -1) {   
      let prevRoute = router.asPath.split("&strtprice");
      router.push(prevRoute[0] + "&strtprice=" + newValue[0] + '&endprice=' + newValue[1] )
    }

    if (router.asPath.indexOf("from=") === -1) { 
      let prevRoute = router.asPath.split("?");
      router.push(prevRoute[0] + "?strtprice=" + newValue[0] + '&endprice=' + newValue[1] )
    }
  };

  React.useEffect(() => {
    const newVal = [...value];
    newVal[1] = parseInt(products.maxRate);
    newVal[0] = 1;
    setValue(newVal);
    setMaxValue(parseInt(products.maxRate));
    setMinValue(1);
  }, [params]);

  return (
    <div>
      <Accordion onChange={handleChange("panel1")}>
        <AccordionSummary aria-controls="panel1d-content" id="panel1d-header">
          <Typography>Price Range</Typography>
        </AccordionSummary>
        <AccordionDetails sx={{ paddingBottom: "5px" }}>
          <Grid container spacing={0}>
            <Grid item sm={6} xs={5}>
              TK {value[0]}
            </Grid>
            <Grid item xs={6} sm={6} sx={{ textAlign: "right" }}>
              TK {value[1]}
            </Grid>
          </Grid>
          <Slider
            getAriaLabel={() => "Temperature range"}
            value={value}
            size="small"
            min={minValue}
            max={maxValue}
            onChange={handleSlider}
            onChangeCommitted={handleSliderCommitted}
            valueLabelDisplay="auto"
            getAriaValueText={valuetext}
          />
        </AccordionDetails>
      </Accordion>
    </div>
  );
}
