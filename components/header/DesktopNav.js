import React from "react";
import ArrowDropDownIcon from "@mui/icons-material/ArrowDropDown";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemText from "@mui/material/ListItemText";
import Link from 'next/link'
// import Image from 'next/image'

export default function DesktopNav({ menu }) {
  const menuLIst = menu.map((m) => {
    return (
      <div className="dropdown" key={m.id} >
        <Link href={'/' + m.slug }>
          <a>
            <span>
              {m.root_category}
              <ArrowDropDownIcon fontSize="small" />
            </span>
          </a>
        </Link>
        <div className="dropdown-content">
          <Grid container justifyContent="center">
            {m.category.map((c) => {
              return (
                <Grid item xs={2} key={c.id}>
                  <Box sx={{ width: "100%", bgcolor: "background.paper" }}>
                    <nav aria-label="main mailbox folders">
                      <List dense>
                        <ListItem disablePadding>
                          <Link href={'/' + m.slug + '/' + c.slug}>
                            <a>
                              <ListItemButton>
                                <b> {c.category} </b>
                              </ListItemButton>
                            </a>
                          </Link>
                        </ListItem>
                        {
                          c.subcategory.map(subCat => {
                            return (
                              <ListItem disablePadding key={subCat.id}>
                                <Link href={'/' + m.slug + '/' + c.slug + '/' + subCat.slug}>
                                  <a>
                                    <ListItemButton>
                                      <ListItemText primary={ subCat.sub_category } />
                                    </ListItemButton>
                                  </a>
                                </Link>
                              </ListItem>
                            )
                          })
                        }
                      </List>
                    </nav>
                  </Box>
                </Grid>
              );
            })}
          </Grid>
        </div>
      </div>
    );
  });

  return (
    <>
      <div className="navbar">
        {menuLIst}
      </div>
    </>
  );
}
