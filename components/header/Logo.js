import React from "react";
import Link from 'next/link'

export default function Logo() {
  return (
    <div className="desktop-logo-container">
      <Link href="/" >
        <img
          src="/common-asset/miah.gif"
          alt="Picture of the author"
        />
      </Link>
    </div>
  );
}
