import React from "react";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import { drawer, setSearchPanel } from "../../redux/menu/menuActions";
import { useDispatch, useSelector } from "react-redux";
import Typography from "@mui/material/Typography";
import Link from "next/link";
import IconButton from "@mui/material/IconButton";
import Badge from "@mui/material/Badge";
import HomeOutlinedIcon from "@mui/icons-material/HomeOutlined";
import LocalPhoneRoundedIcon from "@mui/icons-material/LocalPhoneRounded";
import SearchIcon from "@mui/icons-material/Search";
import MenuIcon from "@mui/icons-material/Menu";
import AccountCircleOutlinedIcon from "@mui/icons-material/AccountCircleOutlined";
import ShoppingBagOutlinedIcon from "@mui/icons-material/ShoppingBagOutlined";
import ShoppingBagMenu from "../menu/ShoppingBagMenu";
import { mobileBagDialog } from "../../redux/shoppingBag/shoppingBagActions";
import { setMobileUserAccount } from "../../redux/auth/authActions";
import AccountMenu from "../menu/AccountMenu";

export default function TopBarDesktop({ menutype }) {
  // hooks
  const dispatch = useDispatch();

  const SgoppingBagLength = useSelector((state) =>
    state.shoppingBag.shoppingCart.reduce((a, b) => a + (b.qty || 0), 0)
  );

  // methods
  const userInfo = useSelector((state) => state.auth.userInfo);

  const openDrawer = () => {
    dispatch(drawer(true));
  };

  const shoppingDialog = () => {
    dispatch(mobileBagDialog(true));
  };

  const mobileUserDialog = () => {
    dispatch(setMobileUserAccount(true));
  };

  const showSearch = () => {
    dispatch(setSearchPanel(true));
  };

  return (
    <>
      <Box
        sx={{
          flexGrow: 1,
          background: "#fbfbfb",
          color: "#000",
          paddingLeft: "5px",
          paddingRight: "10px",
          paddingTop: "12px",
          position: "relative",
          height: 50,
        }}
      >
        {menutype === "top" ? (
          <Grid container spacing={2}>
            <Grid item xs={6}>
              <Box sx={{ display: { xs: "none", sm: "block" } }}>
                <LocalPhoneRoundedIcon />
                <Typography variant="body2" component="span" gutterBottom>
                  <span className="icontext">
                    <a href="tel:+8801313767678">+8801313767678</a>
                  </span>
                </Typography>
              </Box>
              <Box sx={{ display: { xs: "block", sm: "none" } }}>
                <IconButton onClick={openDrawer} aria-label="homeIcon">
                  <MenuIcon />
                </IconButton>
                {/* <Typography
                  variant="body2"
                  component="span"
                  gutterBottom
                  onClick={openDrawer}
                >
                  <span className="icontext">MENU</span>
                </Typography> */}
              </Box>
            </Grid>
            <Grid item xs={6} sx={{ textAlign: "right"}}>
              <Box sx={{ display: { xs: "none", sm: "block" } }}>
                <Badge
                  onClick={showSearch}
                  badgeContent=" "
                  sx={{ cursor: "pointer" }}
                >
                  <SearchIcon />
                </Badge>
                <AccountMenu />
                <ShoppingBagMenu />
              </Box>

              {/* ======================== mobile shopping bag dialog, account dialog ==================== */}
              {/* <Box sx={{ display: { xs: "block", sm: "none" } }}>
                <Badge
                  onClick={showSearch}
                  badgeContent=" "
                  sx={{ cursor: "pointer" }}
                >
                  <SearchIcon />
                </Badge>
                <Badge
                  onClick={shoppingDialog}
                  badgeContent={SgoppingBagLength}
                  sx={{ cursor: "pointer" }}
                >
                  <ShoppingBagOutlinedIcon />
                </Badge>
                {userInfo.token ? (
                  <Link href="/dashboard/account-information">
                    <a>
                      <Badge
                        sx={{ cursor: "pointer" }}
                        color="secondary"
                        badgeContent=""
                        variant="dot"
                      >
                        <AccountCircleOutlinedIcon sx={{ marginLeft: "5px" }} />
                      </Badge>
                    </a>
                  </Link>
                ) : (
                  <Badge
                    onClick={mobileUserDialog}
                    sx={{ cursor: "pointer" }}
                    color="secondary"
                  >
                    <AccountCircleOutlinedIcon sx={{ marginLeft: "5px" }} />
                  </Badge>
                )}
              </Box> */}
            </Grid>
          </Grid>
        ) : (
          <Grid container spacing={2} className="textCenter">
            <Grid item xs={3} className="paddingTop10">
              <Link href="/">
                <a>
                  <IconButton aria-label="homeIcon">
                    <HomeOutlinedIcon />
                  </IconButton>
                </a>
              </Link>
            </Grid>
            <Grid item xs={3} className="paddingTop10">
              <IconButton onClick={showSearch} aria-label="homeIcon">
                <SearchIcon />
              </IconButton>
            </Grid>
            <Grid item xs={3} className="paddingTop10">
              <Badge
                onClick={shoppingDialog}
                badgeContent={SgoppingBagLength}
                overlap="circular"
              >
                <IconButton aria-label="homeIcon">
                  <ShoppingBagOutlinedIcon />
                </IconButton>
              </Badge>
            </Grid>
            <Grid item xs={3} className="paddingTop10">
              {userInfo.token ? (
                <Link href="/dashboard/account-information">
                  <a>
                    <Badge
                      sx={{ cursor: "pointer" }}
                      color="secondary"
                      badgeContent=""
                      variant="dot"
                      overlap="circular"
                    >
                      <IconButton aria-label="homeIcon">
                        <AccountCircleOutlinedIcon />
                      </IconButton>
                    </Badge>
                  </a>
                </Link>
              ) : (
                <IconButton aria-label="homeIcon" onClick={mobileUserDialog}>
                  <AccountCircleOutlinedIcon />
                </IconButton>
              )}
            </Grid>
          </Grid>
        )}

        {menutype === "top" ? (
          <Box
            sx={{
              backgroud: "rgba(0,0,0,.15)",
              position: "absolute",
              top: "-5px",
              left: "50%",
              background: "transparent",
              textAlign: "center",
              transform: 'translateX(-50%)',
              display: 'inline-block'
            }}
          >
            <Link href="/">
              <a>
                <img
                  src="/common-asset/logo-design-no-change-2.gif"
                  alt="Picture of the author"
                  width="150px"
                />
              </a>
            </Link>
          </Box>
        ) : null}
      </Box>
    </>
  );
}
