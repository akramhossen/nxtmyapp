import React from 'react';
import { Card, CardHeader } from "@mui/material";

export default function CardContaier({title, children}) {
  return (
    <Card>
      <CardHeader
        sx={{ background: "rgba(0,0,0,1)", color: "#fff", padding: '5px 10px' }}
        subheader={title}
      />
        { children }
    </Card>
  );
}
