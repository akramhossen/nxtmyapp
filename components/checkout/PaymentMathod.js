import * as React from "react";
import Radio from "@mui/material/Radio";
import RadioGroup from "@mui/material/RadioGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormControl from "@mui/material/FormControl";
import { CashPayment, DigitalPayment } from "./IconLabel";
import { useFormikContext } from "formik";
import { useSelector } from "react-redux";

export default function PaymentMathod({ handleDigilatDicount }) {
  // hooks
  const siteOption = useSelector((state) => state.menu.siteOptions);
  const totalAmount = useSelector((state) =>
    state.shoppingBag.shoppingCart.reduce((a, b) => a + (b.amount || 0), 0)
  );

  const { setFieldValue } = useFormikContext();
  const [value, setValue] = React.useState("cash");

  const handleChange = (event) => {
    setValue(event.target.value);
    setFieldValue("paymentType", event.target.value);
    if (event.target.value === "digital") {
      handleDigilatDicount(
        (totalAmount * Number(siteOption.cardPaymentDiscount)) / 100
      );
    } else {
      handleDigilatDicount(0);
    }
  };

  return (
    <FormControl>
      <RadioGroup
        aria-labelledby="demo-controlled-radio-buttons-group"
        name="controlled-radio-buttons-group"
        value={value}
        onChange={handleChange}
      >
        <FormControlLabel
          value="cash"
          control={<Radio />}
          label={<CashPayment />}
        />
        <FormControlLabel
          value="digital"
          control={<Radio />}
          label={<DigitalPayment />}
        />
      </RadioGroup>
    </FormControl>
  );
}
