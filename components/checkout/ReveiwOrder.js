import React from 'react'
import { useSelector } from "react-redux";
import { Box } from "@mui/system";
import { Grid } from "@mui/material";
import Image from "next/image";
import Divider from '@mui/material/Divider';
import CouponCard from './CouponCard';

export default function ReveiwOrder() {
  const shoppingCart = useSelector((state) => state.shoppingBag.shoppingCart)
  const totalAmount = useSelector((state) => state.shoppingBag.shoppingCart.reduce((a, b) => a + ((b.amount) || 0), 0))
  return (
    <>
      <p>Estimate Delivery Date:</p>
      <p><b>January 22nd - January 29th 2022</b></p>
      <h4 style={{textAlign: 'right'}}>Subtotal: Tk {totalAmount}</h4>

      <Box>
        {
          shoppingCart.map((item, pos) => {
            return(
              <Grid container key={pos} spacing={2}>
                <Grid item md={3} xs={12}>
                  <Image
                    src={`https://images.miah.shop/product/${item.image}`}
                    alt="Picture of the author"
                    width={300}
                    height={300}
                    layout="responsive"
                  />
                </Grid>
                <Grid item md={4} xs={12}>
                  <p><b>{item.name}</b></p>
                  <p>Qty: {item.qty}</p>
                  <p>Unit Price: {item.unitPrice}</p>
                </Grid>
                <Grid item md={5} xs={12}>
                  <Box sx={{textAlign: 'right'}}>
                    <p><b>Tk {item.amount}</b></p>
                  </Box>
                </Grid>
              </Grid>
            )
          })
        }
        <br/>
        <Divider />
        <br />
        <br />
        <CouponCard />
      </Box>
    </>
  )
}
