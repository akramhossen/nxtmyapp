import React, { useState } from "react";
import { Box } from "@mui/system";
import Accordion from "@mui/material/Accordion";
import AccordionSummary from "@mui/material/AccordionSummary";
import AccordionDetails from "@mui/material/AccordionDetails";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import Grid from "@mui/material/Grid";
import TextField from "@mui/material/TextField";
import Divider from "@mui/material/Divider";
import { MiahButton } from "../button/MiahButton";
import commonService from "../../service/menu/commonService";
import { useDispatch, useSelector } from "react-redux";
import { setCoupon, setCredit } from "../../redux/checkout/checkoutActions";
import Chip from "@mui/material/Chip";

export default function CouponCard() {
  // =============hooks===============
  const dispatch = useDispatch();
  const userInfo = useSelector((state) => state.auth.userInfo);
  const couponDiscountObj = useSelector((state) => state.checkout.coupon);
  const totalAmount = useSelector((state) =>
    state.shoppingBag.shoppingCart.reduce((a, b) => a + (b.amount || 0), 0)
  );

  // ==============state===============
  const [couponCode, setCouponCode] = useState("");
  const [creditAmount, setCreditAmount] = useState("");
  const [couponError, setCouponError] = useState("");
  const [creditError, setCreditError] = useState("");

  // ==============methods============

  const handleDelete = () => {
    dispatch(
      setCoupon({
        code: "",
        discountAmount: 0,
      })
    );
    setCouponError('')
  };

  const handleOnChange = (event) => {
    setCouponCode(event.target.value);
  };

  const handleCreditAmount = (event) => {
    setCreditAmount(event.target.value);
  };

  const submitCoupon = () => {
    if (couponCode !== "") {
      setCouponError("");
      const body = {
        subTotal: totalAmount,
        couponCode: couponCode,
      };
      commonService
        .postAuthData("couponDiscount", body, userInfo.token)
        .then((response) => {
          if (response.data.status === 1) {
            dispatch(
              setCoupon({
                code: couponCode,
                discountAmount: response.data.discount,
              })
            );
            setCouponCode("");
          } else {
            dispatch(setCoupon({ code: "", discountAmount: 0 }));
            setCouponError(response.data.msg);
            setCouponCode("");
          }
          console.log(response.data);
        })
        .catch((error) => {
          console.log(error);
        });
    }
  };

  const submitCredit = () => {
    setCouponError("");
    const body = {
      amount: creditAmount,
      creditAmountApply: true,
    };
    commonService
      .postAuthData("check_creditAmount", body, userInfo.token)
      .then((response) => {
        if (response.data.status === 1) {
          dispatch(
            setCredit({
              amount: creditAmount,
              creditAmountApply: true,
            })
          );
          setCreditAmount("");
        } else {
          dispatch(setCredit({ amount: 0, creditAmountApply: false }));
          setCreditError(response.data.msg);
          setCreditAmount("");
        }
        console.log(response.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <>
      <Accordion>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
        >
          <p style={{ margin: "0px" }}>
            <b> Apply Points/Credits/Gift Card </b>
          </p>
        </AccordionSummary>
        <AccordionDetails>
          <Box>
            <p style={{ margin: "0px", marginBottom: "10px" }}>
              <b>
                <small>Coupon</small>
              </b>
            </p>
            <Grid container justifyContent="flex-end" spacing={2}>
              <Grid item xs={12}>
                <TextField
                  value={couponCode}
                  onChange={handleOnChange}
                  fullWidth
                  size="small"
                  label="Enter Coupon"
                  variant="outlined"
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                {couponDiscountObj.code.length ? (
                  <Chip
                    label={couponDiscountObj.code + " is applyed"}
                    onDelete={handleDelete}
                  />
                ) : null}
              </Grid>
              <Grid item xs={12} sm={6}>
                <div>
                  <p className="textCenter textRed">{couponError}</p>
                </div>
                <MiahButton methodFromParent={submitCoupon}>Submit</MiahButton>
              </Grid>
            </Grid>
          </Box>
          <br />
          <br />
          <Divider />
          <br />

          {userInfo.token ? (
            <Box>
              <p style={{ margin: "0px", marginBottom: "10px" }}>
                <b>
                  <small>Credit</small>
                </b>
              </p>
              <Grid container justifyContent="flex-end" spacing={2}>
                <Grid item xs={12}>
                  <TextField
                    value={creditAmount}
                    onChange={handleCreditAmount}
                    fullWidth
                    type="number"
                    size="small"
                    label="Enter Credit Amount"
                    variant="outlined"
                  />
                </Grid>
                <Grid item xs={12} sm={4}>
                  <div>
                    <p className="textCenter textRed">{creditError}</p>
                  </div>
                  <MiahButton methodFromParent={submitCredit}>
                    Apply Credit
                  </MiahButton>
                </Grid>
              </Grid>
            </Box>
          ) : null}
        </AccordionDetails>
      </Accordion>
    </>
  );
}
