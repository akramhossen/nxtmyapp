import React, { useEffect, useState } from "react";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import Divider from "@mui/material/Divider";
import Grid from "@mui/material/Grid";
import CheckboxWraper from "../formUI/checkbox/CheckboxWraper";
import { useSelector } from "react-redux";
import OfferDiscount from "./OfferDiscount";

export default function OrderSummary({
  digitalDiscount,
  shippingCharge,
  couponDiscount,
}) {
  // hooks
  const totalAmount = useSelector((state) =>
    state.shoppingBag.shoppingCart.reduce((a, b) => a + (b.amount || 0), 0)
  );
  const billingAreas = useSelector(
    (state) => state.checkout.formInitialValue.billingArea
  );
  const offerDiscount = useSelector((state) => state.checkout.offerDiscount);
  const couponDiscountObj = useSelector((state) => state.checkout.coupon);
  const creditDiscountObj = useSelector((state) => state.checkout.credit);
  const siteOptions = useSelector((state) => state.menu.siteOptions);

  // local state
  const [tax, seTaxt] = useState(0);

  // methods

  const calculateTex = () => {
    if (totalAmount > 0) {
      const tax = (siteOptions.tax * totalAmount) / 100;
      seTaxt(tax);
    }
  };

  // site effects
  useEffect(() => {
    calculateTex();
  }, [totalAmount]);

  return (
    <div>
      <List component="nav" aria-label="mailbox folders">
        <ListItem>
          <Grid container spacing={0}>
            <Grid item xs={6}>
              <b>Subtotal</b>
            </Grid>
            <Grid item xs={6}>
              <b>Tk {totalAmount}</b>
            </Grid>
          </Grid>
        </ListItem>
        <Divider />
        <ListItem>
          <Grid container spacing={0}>
            <Grid item xs={6}>
              Shipping
            </Grid>
            <Grid item xs={6}>
              Tk {shippingCharge}
            </Grid>
          </Grid>
        </ListItem>
        <Divider />
        <ListItem>
          <Grid container spacing={0}>
            <Grid item xs={6}>
              Tax
            </Grid>
            <Grid item xs={6}>
              Tk {tax}
            </Grid>
          </Grid>
        </ListItem>
        <Divider />
        {offerDiscount.discountValue > 0 ? (
          <ListItem>
            <Grid container spacing={0}>
              <Grid item xs={6}>
                <span className="textRed">Discount</span>
              </Grid>
              <Grid item xs={6}>
                <span className="textRed">-Tk {offerDiscount.discountValue}</span>
              </Grid>
            </Grid>
          </ListItem>
        ) : null}
        {offerDiscount.discountValue > 0 ? <Divider /> : null}
        {couponDiscountObj.discountAmount > 0 ? (
          <ListItem>
            <Grid container spacing={0}>
              <Grid item xs={6}>
                <span className="textRed">Coupon</span>
                <span className="textRed"> ({couponDiscountObj.code})</span>
              </Grid>
              <Grid item xs={6}>
                <span className="textRed">
                  -Tk {couponDiscountObj.discountAmount}
                </span>
              </Grid>
            </Grid>
          </ListItem>
        ) : (
          <ListItem>
            <Grid container spacing={0}>
              <Grid item xs={6}>
                <span>Coupon</span>
              </Grid>
              <Grid item xs={6}>
                <span>Tk 0</span>
              </Grid>
            </Grid>
          </ListItem>
        )}
        <Divider />
        {creditDiscountObj.amount > 0 ? (
          <ListItem>
            <Grid container spacing={0}>
              <Grid item xs={6}>
                <span className="textRed">Credit</span>
              </Grid>
              <Grid item xs={6}>
                <span className="textRed">-Tk 10</span>
              </Grid>
            </Grid>
          </ListItem>
        ) : 
          null
        }
        <Divider />
        {offerDiscount.shippingFree ? (
          <ListItem>
            <Grid container spacing={0}>
              <Grid item xs={6}>
                <span className="textRed">Shipping Discount</span>
              </Grid>
              <Grid item xs={6}>
                <span className="textRed">-Tk 20</span>
              </Grid>
            </Grid>
          </ListItem>
        ) : null}
        {offerDiscount.shippingFree ? <Divider /> : null}
        
        {digitalDiscount > 0 ? (
          <ListItem>
            <Grid container spacing={0}>
              <Grid item xs={6}>
                <span className="textRed">Digital Payment Discount</span>
              </Grid>
              <Grid item xs={6}>
                <span className="textRed">Tk {digitalDiscount}</span>
              </Grid>
            </Grid>
          </ListItem>
        ) : (
          <ListItem>
            <Grid container spacing={0}>
              <Grid item xs={6}>
                Digital Payment Discount
              </Grid>
              <Grid item xs={6}>
                Tk {digitalDiscount}
              </Grid>
            </Grid>
          </ListItem>
        )}
        <Divider />
        <ListItem>
          <Grid container spacing={0}>
            <Grid item xs={6}>
              <b>Total</b>
            </Grid>
            <Grid item xs={6}>
              <b>
                Tk{" "}
                {
                totalAmount +
                  tax +
                  parseInt(shippingCharge) -
                  digitalDiscount -
                  offerDiscount.discountValue -
                  creditDiscountObj.amount -
                  couponDiscountObj.discountAmount
                }
              </b>
            </Grid>
          </Grid>
        </ListItem>
        <Divider />
      </List>
      <CheckboxWraper />
      <OfferDiscount />
    </div>
  );
}
