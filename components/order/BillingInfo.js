import React from "react";

function BillingInfo({ orderDetails }) {
  return (
    <>
      {orderDetails ? (
        <div style={{ padding: "0 15px" }}>
          <p>
            {" "}
            <b>Billing Address</b>
          </p>
          <div>
            <p className="marginBottomZero">{orderDetails.customer_name}</p>
            <p className="marginTopZero">{orderDetails.phone}</p>
            <p dangerouslySetInnerHTML={{ __html: orderDetails.b_address }} />
          </div>
        </div>
      ) : null}
    </>
  );
}

export default BillingInfo;
