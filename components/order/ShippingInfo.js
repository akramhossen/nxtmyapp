import React from 'react'

function ShippingInfo({orderDetails}) {
  return (
    <>
      {
        orderDetails
        ?
        (
          <div>
            <p> <b>Shipping Address</b></p>
            <div>
              <p className='marginBottomZero'>{orderDetails.shipping_cust_name}</p>
              <p className='marginTopZero'>{orderDetails.s_phone}</p>
              <p dangerouslySetInnerHTML={{__html: orderDetails.s_address}} />
            </div>
          </div>
        )
        :
          null
      }
    </>
  )
}

export default ShippingInfo