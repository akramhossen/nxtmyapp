import React, { useState } from "react";
import Image from "next/image";
import { Grid, Box } from "@mui/material";
import Link from "next/link";
// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/css";
import "swiper/css/navigation";
// import Swiper core and required modules
import SwiperCore, { FreeMode, Navigation, Thumbs } from "swiper";
import { IMAGE_URL } from "../../service/serviceConfig";
import WishIcon from "./WishIcon";
import { useDispatch } from "react-redux";
import { setRecentView } from "../../redux/auth/authActions";

// install Swiper modules
SwiperCore.use([Navigation]);

const shimmer = (w, h) => `
<svg width="${w}" height="${h}" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
  <defs>
    <linearGradient id="g">
      <stop stop-color="#f5f3f3" offset="20%" />
      <stop stop-color="#d3d1d1" offset="50%" />
      <stop stop-color="#f5f3f3" offset="70%" />
    </linearGradient>
  </defs>
  <rect width="${w}" height="${h}" fill="#f5f3f3" />
  <rect id="r" width="${w}" height="${h}" fill="url(#g)" />
  <animate xlink:href="#r" attributeName="x" from="-${w}" to="${w}" dur="2.5s" repeatCount="indefinite"  />
</svg>`;

const toBase64 = (str) =>
  typeof window === "undefined"
    ? Buffer.from(str).toString("base64")
    : window.btoa(str);

export default function SingleProduct({ product, variant }) {
  // hooks
  const dispatch = useDispatch();

  // local state
  const [productSlider, setProductSlider] = useState(variant[0]);
  const [thumbsSwiper, setThumbsSwiper] = useState(null);

  // methods

  const recentViewProduct = () => {
    const temp = {
      id: product.id,
      name: product.name,
      slug: product.slug,
      back_img: product.back_img,
    };
    dispatch(setRecentView(temp));
  };
  const setSlider = (slider) => {
    setProductSlider(slider);
  };
  return (
    <Grid item md={3} xs={6}>
      <div className="single-product-container">
        <div className="single-product-inner">
          <div className="single-prod-slide-inner" onClick={recentViewProduct}>
            <Link href={`/product/${product.slug}`}>
              <a>
                <Swiper
                  thumbs={{ swiper: thumbsSwiper }}
                  navigation={true}
                  className="mainSlider"
                  speed={1000}
                  modules={[FreeMode, Navigation, Thumbs]}
                >
                  {productSlider.map((item) => {
                    return (
                      <SwiperSlide key={item.id}>
                        <Image
                          src={`${IMAGE_URL}m_thumb/${item.img}`}
                          alt={item.img_title}
                          width={300}
                          height={300}
                          layout="responsive"
                          placeholder="blur"
                          blurDataURL={`data:image/svg+xml;base64,${toBase64(
                            shimmer(700, 475)
                          )}`}
                        />
                      </SwiperSlide>
                    );
                  })}
                </Swiper>
              </a>
            </Link>
          </div>
          <Box
            sx={{
              display: { xs: "block", sm: "none" },
              height: "10px",
              overflow: "hidden",
            }}
          >
            <Swiper
              className="thumbslide"
              onSwiper={setThumbsSwiper}
              slidesPerView={5}
              spaceBetween={5}
              freeMode={true}
              navigation={false}
              watchSlidesProgress={true}
              modules={[FreeMode, Navigation, Thumbs]}
              speed={1000}
            >
              {productSlider.map((item) => {
                return (
                  <SwiperSlide key={item.id}>
                    <Box className="slide-thumb"></Box>
                  </SwiperSlide>
                );
              })}
            </Swiper>
          </Box>
          <Box sx={{ display: { xs: "none", sm: "block" } }}>
            <div className="prod-color-variation-cont">
              <Swiper
                slidesPerView={3}
                spaceBetween={5}
                navigation={true}
                speed={1000}
                className="color-variation-Swiper"
              >
                {variant.map((item, pos) => {
                  return (
                    <SwiperSlide key={pos}>
                      <Image
                        src={`${IMAGE_URL}s_thumb/${
                          item[0] ? item[0].img : ""
                        }`}
                        alt={item[0] ? item[0].img_title : ""}
                        width={300}
                        height={300}
                        layout="responsive"
                        placeholder="blur"
                        blurDataURL="/homeAsset/bckgnd.png"
                        onClick={() => setSlider(item)}
                      />
                    </SwiperSlide>
                  );
                })}
              </Swiper>
            </div>
          </Box>
          <span className="offer-card">{product.ProdyctType}</span>
          <WishIcon product={product} />
          <div className="info-cont">
            <p>
              <b>{product.name}</b>
            </p>
            <p>
              <small>{product.name_bangla}</small>
            </p>
            <p className="marginTopZero">
              <small>
                <b>TK {product.sales_cost}</b>
              </small>
            </p>
            <p>
              <small>
                <b>{variant.length} Colors</b>
              </small>
            </p>
          </div>
          <Box
            sx={{ display: { xs: "block", md: "none" }, marginBottom: "20px" }}
          >
            <Swiper
              slidesPerView={5}
              spaceBetween={5}
              navigation={true}
              speed={1000}
              className="color-variation-Swiper"
            >
              {variant.map((item, pos) => {
                return (
                  <SwiperSlide key={pos}>
                    <Image
                      src={`${IMAGE_URL}m_thumb/${item[0] ? item[0].img : ""}`}
                      alt={item[0] ? item[0].img_title : ""}
                      width={300}
                      height={300}
                      layout="responsive"
                      placeholder="blur"
                      blurDataURL="/homeAsset/bckgnd.png"
                      onClick={() => setSlider(item)}
                    />
                  </SwiperSlide>
                );
              })}
            </Swiper>
          </Box>
        </div>
      </div>
    </Grid>
  );
}
