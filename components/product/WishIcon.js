import React from "react";
import FavoriteBorderIcon from "@mui/icons-material/FavoriteBorder";
import FavoriteIcon from '@mui/icons-material/Favorite';
import commonService from "../../service/menu/commonService";
import { useSelector } from "react-redux";


export default function WishIcon({product}) {

  // hooks
  const userInfo = useSelector((state) => state.auth.userInfo);

  // state
  const [wish, setWish] = React.useState(false)

  // methods
  const handleWishIcon = () => {
    const api = product.wishList === 1 ?  'removeWishList' : 'addWishlist'
    commonService
      .authGetData(`${api}/${product.id}`, userInfo.token)
      .then((res) => {
        if (res.data.data === 'success') {
          setWish(false)
        }
        if (res.data.data === 'Successfully Added') {
          setWish(true)
        }
        console.log(res);
      })
      .catch((error) => {
        console.log(error);
      });
  }

  // side effects
  React.useEffect(() => {
    if(product.wishList == 1) {
      setWish(true)
    }
  }, [])
  
  return (
    <span className="product-wislist-icon">
      {
        wish ?
        <FavoriteIcon onClick={handleWishIcon} color="action" />
        :
        <FavoriteBorderIcon onClick={handleWishIcon} color="action" />
      }
    </span>
  );
}
