
// import React, { useRef, useState } from "react"
// // Import Swiper React components
// import { Swiper, SwiperSlide } from "swiper/react"
// import Grid from '@mui/material/Grid'

// // Import Swiper styles
// import "swiper/css";
// import "swiper/css/zoom"
// import "swiper/css/navigation"
// import "swiper/css/pagination"

// // import Swiper core and required modules
// import SwiperCore, {
//   Zoom,Navigation,Pagination
// } from 'swiper';

// // install Swiper modules
// SwiperCore.use([Zoom,Navigation,Pagination]);


// export default function FullScreenSlider({varitation}) {
  
  
  
//   return (
//     <>
//     <Grid
//       container
//       spacing={2}
//       justifyContent="center"
//       alignItems="center"
//     >
//       <Grid item xs={8} md={8}>
//         <Swiper
//           style={{'--swiper-navigation-color': '#fff','--swiper-pagination-color': '#fff'}}
//           zoom={true}
//           navigation={true}
//           pagination={{"clickable": true }}
//           className="mySwiper"
//         >
//           {
//             varitation.map(item => {
//               return (
//                 <SwiperSlide key={item.id}>
//                   <div className="swiper-zoom-container">
//                     <img
//                       width="100%"
//                       src={`https://images.miah.shop/product/${item.img}`}
//                     />
//                   </div>
//                 </SwiperSlide>
//               )
//             })
//           }
//         </Swiper>
//       </Grid>
//     </Grid>
//     </>
//   )
// }