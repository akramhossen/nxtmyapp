import React from "react";
import Image from "next/image";
import Link from "next/link";
import { IMAGE_URL } from "../../../service/serviceConfig";

export default function RelatedProducts({ product }) {
  return (
    <>
      <div className="related-prd-container">
        <Link href={`/product/${product.slug}`}>
          <a>
            <div className="related-frnt-image">
              <Image
                src={`${IMAGE_URL}/m_thumb/${product.front_img}`}
                alt={product.img_title}
                width={300}
                height={300}
                layout="responsive"
                placeholder="blur"
                blurDataURL="/homeAsset/bckgnd.png"
              />
            </div>
            <div className="related-back-image">
              <Image
                src={`${IMAGE_URL}/m_thumb/${product.back_img}`}
                alt={product.img_title}
                width={300}
                height={300}
                layout="responsive"
                placeholder="blur"
                blurDataURL="/homeAsset/bckgnd.png"
              />
            </div>
            <div className="miahfornt parentMarginZero" style={{paddingBottom:'20px',paddingTop:'10px', fontSize: '.89rem'}}>
              <p> {product.name} </p>
              <p> {product.name_bangla} </p>
              <p> {product.sales_cost} </p>
            </div>
          </a>
        </Link>
      </div>
    </>
  );
}
