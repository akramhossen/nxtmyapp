import React, { useState, useEffect } from "react";
import Image from "next/image";
import { useRouter } from "next/router";
import Snackbar from "@mui/material/Snackbar";
import { addToBag } from "../../../redux/shoppingBag/shoppingBagActions";
import {
  Button,
  Grid,
  Box,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  TextField,
  IconButton,
  ToggleButton,
  ToggleButtonGroup,
} from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";
import { styled } from "@mui/material/styles";
import { useDispatch, useSelector } from "react-redux";
import {
  AddToBagButton,
  MiahLoadingButton,
  MiahButton,
} from "../../button/MiahButton";
import commonService from "../../../service/menu/commonService";

const ButtonQty = styled(Button)({
  boxShadow: "none !important",
  outline: "none !important",
  textTransform: "none",
  padding: "6px",
  border: "1px solid",
  lineHeight: 0.5,
  borderRadius: 0,
  minWidth: "40px",
  margin: "0px 5px",
  borderColor: "#000000",
  color: "#000",
  "&:hover": {
    backgroundColor: "rgba(0,0,0,0.1)",
  },
});

function AddToBagContainer({ products, prodVari }) {
  // hooks
  const dispatch = useDispatch();
  const router = useRouter();
  const shoppingBag = useSelector((state) => state.shoppingBag.shoppingCart);

  // local state
  const [dialog, setDialog] = useState(false);
  const [stockOut, setStockOut] = useState(false);
  const [bntloading, setBtnLoading] = useState(false);
  const [cutFabDesc, setCutFabDesc] = useState("");
  const [cutFabQty, setCutFabQty] = useState(1);
  const [item, setItem] = useState({});
  const [error, setError] = useState({ error: false, msg: "" });
  const [itemSize, setItemSize] = useState("");
  const [isSizeSelected, setIsSizeSelected] = useState(true);
  const [itemSku, setItemSku] = useState(prodVari.sku);
  const [itemSkuID, setItemSkuID] = useState(prodVari.sku_id);

  // methods
  const increaseQty = () => {
    setCutFabQty((prv) => prv + 1);
    createItem()
    console.log(cutFabQty);
  };
  const decreaseQty = () => {
    setCutFabQty((prv) => (prv > 1 ? prv - 1 : prv));
    createItem()
    console.log(cutFabQty);
  };
  const handeCutFabDesc = (e) => {
    setCutFabDesc(e.target.value);
  };
  const createItem = () => {
    let itemObj = {};
    // itemObj.id = prodVari.sku;
    itemObj.id = itemSku;
    itemObj.proID = products[0].id;
    itemObj.category_id = products[0].category_id;
    itemObj.name = products[0].name;
    itemObj.weight = parseFloat(products[0].weight);
    itemObj.image = prodVari.vImage[0].img;
    // itemObj.skuId = prodVari.sku_id;
    itemObj.skuId = itemSkuID;
    itemObj.cutDescription = "";
    itemObj.unitPrice = parseFloat(products[0].sales_cost);
    itemObj.qty = Number(products[0].category_id) === 7 ? cutFabQty : 1;
    itemObj.size = itemSize;
    itemObj.amount = parseFloat(products[0].sales_cost);
    itemObj.stock = false;
    setItem(itemObj);
    console.log(itemObj);
  };

  const trackChanged = () => {
    console.log("changes done");
  };
  const addItemToBag = () => {

    if (prodVari.vSize.length && itemSize === '') {
      setIsSizeSelected(false)
      return
    }

    setBtnLoading(true);
    createItem();
    setError({ ...error, error: false, msg: "" });
    const prodExist = shoppingBag.find((item) => item.id === prodVari.sku);
    if (prodExist) {
      const body = {
        sku: prodVari.sku,
        qty:
          Number(products[0].category_id) === 7
            ? prodExist.qty + cutFabQty
            : prodExist.qty + 1,
      };
      commonService
        .postData("productStockChk", body)
        .then((res) => {
          if (res.data.status === true) {
            dispatch(addToBag(item));
            setBtnLoading(false);
            // setDialog(true);
            trackChanged();
          } else {
            setBtnLoading(false);
            setStockOut(true);
            setError({ ...error, error: true, msg: res.data.data });
          }
        })
        .catch((error) => {
          console.log("errror", error);
        });
    } else {
      dispatch(addToBag(item));
    }
  };

  const handleSize = (size) => {
    setItemSku(size.sku);
    setItemSkuID(size.sku_id);
    setItemSize(size.size);
    setIsSizeSelected(true)
  };

  const action = (
    <React.Fragment>
      <IconButton
        size="small"
        aria-label="close"
        color="inherit"
        onClick={() => setStockOut(false)}
      >
        <CloseIcon fontSize="small" />
      </IconButton>
    </React.Fragment>
  );

  const handleDialogClose = () => {
    setDialog(false);
  };
  const goToCheckout = () => {
    setDialog(false);
    router.push("/Checkout");
  };

  // side effects
  useEffect(() => {
    setItemSku(prodVari.sku);
    setItemSkuID(prodVari.sku_id);
    createItem();
    console.log(products[0].category_id);
  }, [products, prodVari]);

  useEffect(() => {
    if (bntloading) {
      setDialog(true);
    }
    setBtnLoading(false);
  }, [shoppingBag]);

  return (
    <>
      {products[0].category_id === 7 ? (
        <Grid container>
          <Grid item md={12} xs={12}>
            <Box sx={{ padding: { xs: "10px 0px" } }}>
              <ButtonQty onClick={decreaseQty}>-</ButtonQty>
              <ButtonQty>{cutFabQty}</ButtonQty>
              <ButtonQty onClick={increaseQty}>+</ButtonQty>
              <Button>YARD</Button>
            </Box>
          </Grid>
          <Grid item md={6} xs={12}>
            <TextField
              fullWidth
              value={cutFabDesc}
              onChange={handeCutFabDesc}
              label="Cut Fabrics Instructions (if any)"
              multiline
            />
          </Grid>
        </Grid>
      ) : null}
      {error.error ? <p style={{ color: "red" }}> {error.msg} </p> : null}
      <br />
      <ToggleButtonGroup exclusive value={itemSize} size="small">
        {prodVari.vSize.map((size, pos) => {
          return (
            <ToggleButton
              sx={{ maxWidth: "50px", minWidth: "50px", marginBottom: "15px" }}
              key={pos}
              value={size.size}
              disabled={size.inventory == 0 ? true : false }
              onClick={() => handleSize(size)}
            >
              {size.size}
            </ToggleButton>
          );
        })}
      </ToggleButtonGroup>
      {
        !isSizeSelected ?
        <div>
          <p className="textRed marginTopZero">Please select your size</p>
        </div>
        :
        null
      }
      {prodVari.inventory > 0 ? (
        <div>
          {bntloading === true ? (
            <MiahLoadingButton>Add To Bag</MiahLoadingButton>
          ) : (
            <AddToBagButton addItemToBag={addItemToBag}>
              Add To Bag
            </AddToBagButton>
          )}
        </div>
      ) : (
        <div>
          <Button variant="contained" fullWidth disabled>
            Out of Stock
          </Button>
        </div>
      )}
      <Box
        sx={{
          backgroundColor: "white",
          position: "fixed",
          bottom: "50px",
          left: 0,
          width: "100%",
          zIndex: "modal",
          padding: "5px",
          boxShadow: "2px 2px 5px rgba(0,0,0,.1)",
          display: { xs: "block", sm: "none" },
        }}
      >
        <Grid container spacing={1}>
          <Grid item xs={2}>
            <Image
              src={`https://images.miah.shop/product/${prodVari.vImage[0].img}`}
              alt="Picture of the author"
              width={50}
              height={50}
              layout="fixed"
              placeholder="blur"
              blurDataURL="/homeAsset/bckgnd.png"
            />
          </Grid>
          <Grid item xs={5}>
            <Box sx={{ textAlign: "center" }}>
              <p className="marginBottomZero marginTopZero">
                <small>Unit Price</small>
              </p>
              <p className="marginBottomZero marginTopZero">
                <b>Tk {products[0].sales_cost}</b>
              </p>
            </Box>
          </Grid>
          <Grid item xs={5}>
            {prodVari.inventory > 0 ? (
              <div>
                {bntloading === true ? (
                  <MiahLoadingButton>Add To Bag</MiahLoadingButton>
                ) : (
                  <AddToBagButton addItemToBag={addItemToBag}>
                    Add To Bag
                  </AddToBagButton>
                )}
              </div>
            ) : (
              <div>
                <Button variant="contained" fullWidth disabled>
                  Out of Stock
                </Button>
              </div>
            )}
          </Grid>
        </Grid>
      </Box>

      {/* ========================= product dialog ===================== */}
      <Dialog
        open={dialog}
        onClose={handleDialogClose}
        fullWidth
        maxWidth="sm"
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          {
            <small>
              <b>Successfully Added To Your Bag</b>
            </small>
          }
        </DialogTitle>
        <DialogContent>
          <Grid container spacing={2}>
            <Grid item sm={6} xs={12}>
              <Image
                src={`https://images.miah.shop/product/${prodVari.vImage[0].img}`}
                alt="Picture of the author"
                width={300}
                height={300}
                layout="responsive"
                placeholder="blur"
                blurDataURL="/homeAsset/bckgnd.png"
              />
            </Grid>
            <Grid item sm={6} xs={12}>
              <p className="marginBottomZero marginTopZero">
                <b>{products[0].name}</b>
              </p>
              <p className="marginBottomZero marginTopZero">
                <b>sku : </b> {prodVari.sku}
              </p>
              <p className="marginBottomZero marginTopZero">
                <b>Unit Price : </b>Tk {products[0].sales_cost}
              </p>
              <p className="marginBottomZero marginTopZero">
                {
                  Number(products[0].category_id) === 7 ?
                  <span>
                    <b>Quantity : </b>{cutFabQty}
                  </span>
                  :
                  <span>
                    <b>Quantity : </b> 1
                  </span>
                }
              </p>
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions>
          <Grid container justifyContent="end" spacing={1}>
            <Grid item sm={12}>
              <Grid container justifyContent="end" spacing={2}>
                <Grid item sm={6} xs={12}>
                  <MiahButton methodFromParent={handleDialogClose}>
                    Continue Shopping
                  </MiahButton>
                </Grid>
                <Grid item sm={6} xs={12}>
                  <MiahButton methodFromParent={goToCheckout}>
                    Checkout
                  </MiahButton>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </DialogActions>
      </Dialog>

      {/* ========================= snakbar for mobile ============================ */}
      <Snackbar
        open={stockOut}
        sx={{ display: { xs: "block", sm: "none" } }}
        autoHideDuration={1000}
        action={action}
        message="Out Of Stock"
      />

      <Snackbar
        open={!isSizeSelected}
        sx={{ display: { xs: "block", sm: "none" } }}
        autoHideDuration={1000}
        action={action}
        message="Please select size"
      />

    </>
  );
}

export default AddToBagContainer;
