import React, { useState, useEffect } from "react";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";
import ZoomInIcon from "@mui/icons-material/ZoomIn";
import Typography from "@mui/material/Typography";

// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/css";
import "swiper/css/zoom";
import "swiper/css/navigation";
import "swiper/css/pagination";

// import required modules
import { Zoom, Navigation, Pagination } from "swiper";

import { IMAGE_URL } from "../../../service/serviceConfig";

export default function ProductZoom({ prodVari, zoomImage, setZoom }) {
  // local state
  const [clickToZoom, setClickToZoom] = useState(true);
  const [isZoomed, setIsZoomed] = useState(false);

  // methods
  const closeZoom = () => {
    setZoom(false);
  };

  const handleZoomChanges = (scale) => {
    if(scale === 1) {
      setIsZoomed(false)
    } else {
      setIsZoomed(true)
    }
  }
  // side effects
  useEffect(() => {
    console.log(prodVari);
    setTimeout(() => {
      setClickToZoom(false);
    }, 2000);
  }, []);

  return (
    <div className="my-prismazoom">
      <span
        className={
          clickToZoom ? "clickToZoom clickFadein" : "clickToZoom clickFadeout"
        }
      >
        <ZoomInIcon fontSize="large" color="warning" />
        <Typography variant="p" gutterBottom component="div">
          Double Click / Scroll to zoom
        </Typography>
      </span>
      <IconButton
        aria-label="delete"
        size="small"
        onClick={closeZoom}
        className="closeButton"
      >
        <CloseIcon fontSize="medium" sx={{ color: "red" }} />
      </IconButton>
      <div className="prismazoom-inner">
        <div className={isZoomed ? 'slideW100': 'slideW50'}>
          <Swiper
            style={{
              "--swiper-navigation-color": "#fff",
              "--swiper-pagination-color": "#fff",
            }}
            zoom={true}
            navigation={true}
            pagination={{
              clickable: true,
            }}
            modules={[Zoom, Navigation, Pagination]}
            onZoomChange={(s,scale,f,e)=> {
              handleZoomChanges(scale)
            }}
            className="myZoomSwiper"
          >
            <SwiperSlide>
              <div className="swiper-zoom-container">
                <img src="https://nxtmyapp.vercel.app/_next/image?url=https%3A%2F%2Fimages.miah.shop%2Fproduct%2FStandard_Saree_SR-1087_300_1.jpg&w=1920&q=75" />
              </div>
            </SwiperSlide>
            <SwiperSlide>
              <div className="swiper-zoom-container">
                <img src="https://nxtmyapp.vercel.app/_next/image?url=https%3A%2F%2Fimages.miah.shop%2Fproduct%2FStandard_Saree_SR-1087_300_2.jpg&w=1920&q=75" />
              </div>
            </SwiperSlide>
            <SwiperSlide>
              <div className="swiper-zoom-container">
                <img src="https://nxtmyapp.vercel.app/_next/image?url=https%3A%2F%2Fimages.miah.shop%2Fproduct%2FStandard_Saree_SR-1087_300_4.jpg&w=1920&q=75" />
              </div>
            </SwiperSlide>
            <SwiperSlide>
              <div className="swiper-zoom-container">
                <img src="https://nxtmyapp.vercel.app/_next/image?url=https%3A%2F%2Fimages.miah.shop%2Fproduct%2FStandard_Saree_SR-1087_300_3.jpg&w=1920&q=75" />
              </div>
            </SwiperSlide>
          </Swiper>
        </div>
      </div>
    </div>
  );
}
