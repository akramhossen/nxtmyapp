import React from "react";
import {
  Container,
  Grid,
  List,
  ListItem,
  Stack,
} from "@mui/material";
import { Box } from "@mui/system";
import FacebookIcon from "@mui/icons-material/Facebook";
import InstagramIcon from "@mui/icons-material/Instagram";
import PinterestIcon from "@mui/icons-material/Pinterest";
import LinkedInIcon from "@mui/icons-material/LinkedIn";
import RoomIcon from "@mui/icons-material/Room";
import MarkunreadOutlinedIcon from "@mui/icons-material/MarkunreadOutlined";
import PhoneAndroidOutlinedIcon from "@mui/icons-material/PhoneAndroidOutlined";
import Link from "next/link";
export default function FooterBottom() {
  return (
    <>
      <Box className="miah-footer" sx={{ background: "#473427", color: "#fff", paddingBottom: 10, paddingTop: 6 }}>
        <Container maxWidth="xxl">
          <Grid container spacing={2} justifyContent="center">
            <Grid item xs={6} sm={2}>
              <img
                src="https://miah.shop/img/miah-logo-white.7a69845a.png"
                alt="miah logo"
                width="100%"
              />
              <Box>
                  A high-quality clothing and fashion brand.
              </Box>
              <Box className="footer-icon-list">
                <Stack direction="row" spacing={2}>
                  <Link href="https://www.facebook.com/MiahAndMiah/">
                    <a target="_blank">
                      <FacebookIcon />
                    </a>
                  </Link>
                  <Link href="https://www.instagram.com/accounts/login/?next=/miahandmiah/">
                    <a target="_blank">
                      <InstagramIcon />
                    </a>
                  </Link>

                  <Link href="https://www.pinterest.com/miahandmiah/_created/">
                    <a target="_blank">
                      <PinterestIcon />
                    </a>
                  </Link>

                  <Link href="https://www.linkedin.com/company/miahandmiah/">
                    <a target="_blank">
                      <LinkedInIcon />
                    </a>
                  </Link>
                </Stack>
              </Box>
            </Grid>
            <Grid item xs={6} sm={2}>
              <List dense>
                <ListItem>
                  <b>Important Links</b>
                </ListItem>
                <ListItem>
                  <Link href="/page/termCondition">
                    <a >Terms & Comditions</a>
                  </Link>
                </ListItem>
                <ListItem>
                  <Link href="/page/company">
                    <a >About us</a>
                  </Link>
                </ListItem>
                <ListItem>
                  <Link href="/page/shippingPolicy">
                    <a >Shipping Policy</a>
                  </Link>
                </ListItem>
                <ListItem>
                  <Link href="/page/privacyPolicy">
                    <a >Privecy Policy</a>
                  </Link>
                </ListItem>
                <ListItem>
                  <Link href="/page/paymentPolicy">
                    <a >Payment Policy</a>
                  </Link>
                </ListItem>
                <ListItem>
                  <Link href="/contact-us">
                    <a>Contact us</a>
                  </Link>
                </ListItem>
              </List>
            </Grid>
            <Grid item xs={6} sm={2}>
              <List dense>
                <ListItem>
                  <b>Category</b>
                </ListItem>

                <ListItem>
                  <Link href="/men">
                    <a>Men</a>
                  </Link>
                </ListItem>

                <ListItem>
                  <Link href="/women">
                    <a>women</a>
                  </Link>
                </ListItem>
                <ListItem>
                  <Link href="/others">
                    <a>Othres</a>
                  </Link>
                </ListItem>
              </List>
            </Grid>
            <Grid item xs={6} sm={2}>
              <List dense>
                <ListItem>
                  <b>Account</b>
                </ListItem>
                <ListItem>
                  <Link href="/dashboard/account-information">
                    <a>My Account</a>
                  </Link>
                </ListItem>

                <ListItem>
                  <Link href="/dashboard/order-history">
                    <a>Order History</a>
                  </Link>
                </ListItem>

                <ListItem>
                  <Link href="/dashboard/wish-list">
                    <a>My Wishlist</a>
                  </Link>
                </ListItem>
                <ListItem>
                  <Link href="/order/order-success?orderid=3999">
                    <a>My Wishlist</a>
                  </Link>
                </ListItem>
              </List>
            </Grid>
            <Grid item xs={12} sm={2}>
              <List dense>
                <ListItem>
                  <b>Contact Info</b>
                </ListItem>
                <ListItem>
                  <Grid container spacing={0}>
                    <Grid item xs={2} sm={2}>
                      <RoomIcon />
                    </Grid>
                    <Grid item xs={10} sm={10}>
                      90/1 Motijheel, City Centre (Level 24), Dhaka 1000,
                      Bangladesh
                    </Grid>
                  </Grid>
                </ListItem>
                <ListItem>
                  <Grid container spacing={0}>
                    <Grid item xs={2} sm={2}>
                      <MarkunreadOutlinedIcon />
                    </Grid>
                    <Grid item xs={10} sm={10}>
                      info@miah.shop
                    </Grid>
                  </Grid>
                </ListItem>
                <ListItem>
                  <Grid container spacing={0}>
                    <Grid item xs={2} sm={2}>
                      <PhoneAndroidOutlinedIcon />
                    </Grid>
                    <Grid item xs={10} sm={10}>
                      +8801313767678
                    </Grid>
                  </Grid>
                </ListItem>
              </List>
            </Grid>
          </Grid>
          <Grid container spacing={0} alignItems="center" alignContent="center">
            <Grid item sm={4} xs={12}>
              <p>© 2022 M/S Miah & Miah Enterprise. All right reserved.</p>
            </Grid>
            <Grid item sm={4} xs={12}>
              <img
                width="100%"
                src="https://miah.shop/img/payment-gateway-01.e5223dfd.png"
                alt="miah payment icon"
              />
            </Grid>
          </Grid>
        </Container>
      </Box>
    </>
  );
}
