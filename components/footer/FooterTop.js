import React from 'react'
import { Box } from '@mui/system'

export default function FooterTop() {
  return (
    <>
      <Box
        className="bottomAnimation"
        sx={{marginTop: 18, padding: '20px 0px 25px 0px', background: '#f6d809', textAlign:'center'}}
      >
        <img src="https://miah.shop/img/fianl.ba4f403c.gif" alt=""/>
      </Box>
    </>
  )
}
