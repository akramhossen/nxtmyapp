import React from 'react'
import Grid from '@mui/material/Grid'
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import { fetchLocations } from '../../redux/checkout/checkoutActions'
import { useSelector, useDispatch } from "react-redux";
import shippingCalculation from '../../service/shippingCalculation/shippingCalculation';
import Card from '@mui/material/Card';

import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import Divider from '@mui/material/Divider';
import {MiahButton} from '../button/MiahButton';

import Link from "next/link";



export default function CaculateShipping() {
  const dispatch = useDispatch();
  const locations = useSelector((state) => state.checkout.locations);
  const BagWight = useSelector((state) => state.shoppingBag.shoppingCart.reduce((a, b) => a + ((b.weight) || 0), 0))
  const totalAmount = useSelector((state) => state.shoppingBag.shoppingCart.reduce((a, b) => a + ((b.amount) || 0), 0))

  const [shippingCharge, setShippingCharge] = React.useState(0);
  const [region, setRegion] = React.useState('');

  const [cities, setCities] = React.useState([]);
  const [city, setCity] = React.useState('');

  const [areas, setAreas] = React.useState([]);
  const [area, setArea] = React.useState('');

  const handleRegionChange = (event) => {
    const {value} = event.target 
    setRegion(value)
    setCities(value.city)
  };

  const handleCityChange = (event) => {
    const {value} = event.target 
    setCity(value)
    setAreas(value.area)
  };

  const handleAreaChange = (event) => {
    const {value} = event.target 
    setArea(value)
    setShippingCharge(shippingCalculation.calculateShipping(value, BagWight))
  };

  React.useEffect(() => {
    dispatch(fetchLocations());
  }, [dispatch]);

  return (
    <Grid container spacing={2}>
        <Grid item xs={12}>
          <h4>Calculate Shipping</h4>
          <FormControl fullWidth>
            <InputLabel style={{background: '#ffffff'}} id="demo-simple-select-label">Select Region</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={region}
              onChange={handleRegionChange}
            >
              {
                locations.map((region, pos) => {
                  return(
                    <MenuItem value={region} key={pos}>{region.name}</MenuItem>
                  )

                })
              }
            </Select>
          </FormControl>
        </Grid>

        <Grid item xs={12} sm={6}>
          <FormControl fullWidth>
            <InputLabel style={{background: '#ffffff'}} id="demo-simple-select-label">Select City</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={city}
              onChange={handleCityChange}
            >
              {
                cities.map((city, pos) => {
                  return(
                    <MenuItem value={city} key={pos}>{city.name}</MenuItem>
                  )

                })
              }
            </Select>
          </FormControl>
        </Grid>

        <Grid item xs={12} sm={6}>
          <FormControl fullWidth>
            <InputLabel style={{background: '#ffffff'}} id="demo-simple-select-label">Select Area</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={area}
              onChange={handleAreaChange}
            >
              {
                areas.map((area, pos) => {
                  return(
                    <MenuItem value={area} key={pos}>{area.name}</MenuItem>
                  )

                })
              }
            </Select>
          </FormControl>
        </Grid>
        <Grid item xs={12}>
          <h4>Order Summary</h4>
          <Card>
          <List component="nav" aria-label="mailbox folders">
            <ListItem >
              <Grid container spacing={0}>
                <Grid item xs={6}>
                  <b>Subtotal</b>
                </Grid>
                <Grid item xs={6}>
                  <b>Tk {totalAmount}</b>
                </Grid>
              </Grid>
            </ListItem>
            <Divider />
            <ListItem >
              <Grid container spacing={0}>
                <Grid item xs={6}>
                  Shipping
                </Grid>
                <Grid item xs={6}>
                  Tk  {shippingCharge}
                </Grid>
              </Grid>
            </ListItem>
            <Divider />
            <ListItem >
              <Grid container spacing={0}>
                <Grid item xs={6}>
                  Discount
                </Grid>
                <Grid item xs={6}>
                  Tk  0
                </Grid>
              </Grid>
            </ListItem>
            <Divider />
            <ListItem >
              <Grid container spacing={0}>
                <Grid item xs={6}>
                  <b>
                    Total
                  </b>
                </Grid>
                <Grid item xs={6}>
                  <b>
                    Tk  {totalAmount + shippingCharge}
                  </b>
                </Grid>
              </Grid>
            </ListItem>
            <Divider />
            <ListItem >
              <Grid container spacing={0}>
                <Grid item xs={12}>
                <Link href="/Checkout">
                  <a><MiahButton>Go to Checkout</MiahButton></a>
                </Link>
                </Grid>
              </Grid>
            </ListItem>
          </List>
          </Card>
        </Grid>
    </Grid>
  )
}
