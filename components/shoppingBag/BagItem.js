import React from "react";
import { Button, Grid } from "@mui/material";
import { styled } from "@mui/material/styles";
import { Box } from "@mui/system";
import Image from "next/image";
import DeleteIcon from "@mui/icons-material/Delete";
import FavoriteIcon from "@mui/icons-material/Favorite";
import { IMAGE_URL } from "../../service/serviceConfig";
import commonService from "../../service/menu/commonService";

const ButtonQty = styled(Button)({
  boxShadow: "none !important",
  outline: "none !important",
  textTransform: "none",
  padding: "6px",
  border: "1px solid",
  lineHeight: 0.5,
  borderRadius: 0,
  minWidth: "40px",
  margin: "0px 5px",
  borderColor: "#000000",
  color: "#000",
  "&:hover": {
    backgroundColor: "rgba(0,0,0,0.1)",
  },
});

export default function BagItem({
  item,
  addItemQty,
  removeItemQty,
  removeFromBag,
}) {
  const checkStock = (item, type) => {
    console.log('item', item.qty);
    const body = {
      sku: item.id,
      qty: type === 'deduct' ? item.qty - 1 : item.qty + 1,
    };
    console.log('body', body.qty);
    commonService
      .postData("productStockChk", body)
      .then((res) => {
        console.log(res.data.status);
        if (res.data.status === true) {
          item.stock = false;
          if (type === "deduct" && item.qty > 1) {
            removeItemQty(item);
          }
          if (type === "add") {
            addItemQty(item);
          }
        } else {
          console.log("stock out");
          item.stock = true;
          if (type === "deduct" && item.qty > 1) {
            removeItemQty(item);
          }
          if (type === "add") {
            addItemQty(item);
          }
        }
      })
      .catch((error) => {
        console.log("errror", error);
      });
  };

  const deductQty = (item) => {
    checkStock(item);
    if (item.qty > 1) {
      removeItemQty(item);
    }
  };
  return (
    <div>
      <Box
        key={item.id}
        py={1}
        sx={{ background: "#eff0f4", textAlign: { xs: "center", md: "left" } }}
      >
        <Grid container>
          <Grid item md={2} xs={12}>
            <Image
              src={`${IMAGE_URL}m_thumb/${item.image}`}
              alt="Picture of the author"
              width={300}
              height={300}
              layout="responsive"
            />
          </Grid>
          <Grid item md={4} xs={12}>
            <p>
              <b>{item.name}</b>
            </p>
            <p style={{ margin: "0px", fontWeight: "bold" }}>
              TK {item.unitPrice}
            </p>
            <p style={{ margin: "0px" }}>
              <small>
                <b>sku: {item.id}</b>
              </small>
            </p>
            <Box sx={{ display: { xs: "none", sm: "block" } }}>
              <Button
                className="removeBtn"
                onClick={() => removeFromBag(item)}
                size="small"
                variant="text"
                endIcon={<DeleteIcon />}
              >
                Remove
              </Button>
              <Button
                className="favBtn"
                onClick={() => removeFromBag(item)}
                size="small"
                variant="text"
                startIcon={<FavoriteIcon />}
              >
                Save item
              </Button>
            </Box>
          </Grid>
          <Grid item md={4} xs={8}>
            <Box sx={{ padding: { xs: "10px 0px" } }}>
              {
                item.qty > 1 ?
                <ButtonQty onClick={() => checkStock(item, "deduct")}>
                  -
                </ButtonQty>
                :
                <ButtonQty>
                  -
                </ButtonQty>
              }
              <ButtonQty>QTY {item.qty}</ButtonQty>
              <ButtonQty onClick={() => checkStock(item, "add")}>+</ButtonQty>
            </Box>
            {item.stock ? (
              <p style={{ color: "red", margin: "0px" }}>
                <small>Product is Out Of Stock</small>
              </p>
            ) : null}
          </Grid>
          <Grid item md={2} xs={4}>
            <Box sx={{ padding: { xs: "10px 0px" } }}>TK {item.amount}</Box>
          </Grid>
          <Grid
            item
            md={12}
            xs={12}
            sx={{ display: { xs: "block", sm: "none" } }}
          >
            <Button
              className="removeBtn"
              onClick={() => removeFromBag(item)}
              size="small"
              variant="text"
              endIcon={<DeleteIcon />}
            >
              Remove
            </Button>
            <Button
              className="favBtn"
              onClick={() => removeFromBag(item)}
              size="small"
              variant="text"
              startIcon={<FavoriteIcon />}
            >
              Save item
            </Button>
          </Grid>
        </Grid>
      </Box>
    </div>
  );
}
