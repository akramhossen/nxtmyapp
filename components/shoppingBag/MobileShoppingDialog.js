import React, { useEffect } from "react";
import CloseIcon from "@mui/icons-material/Close";
import Slide from "@mui/material/Slide";
import { connect } from "react-redux";
import { useRouter } from "next/router";
import {
  Dialog,
  DialogContent,
  AppBar,
  Toolbar,
  IconButton,
  Typography,
  Box,
  Grid,
} from "@mui/material";
import {
  addItemQty,
  removeItemQty,
  removeFromBag,
  mobileBagDialog,
} from "../../redux/shoppingBag/shoppingBagActions";
import BagItem from "./BagItem";

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="left" ref={ref} {...props} />;
});

const MobileShoppingDialog = ({
  cartItems,
  dialog,
  addItemQty,
  removeItemQty,
  removeFromBag,
  mobileBagDialog,
}) => {
  // hooks
  const router = useRouter();
  const totalAmount = cartItems.reduce((a, b) => a + ((b.amount) || 0), 0)

  // methods

  const goToCheckout = () => {
    router.push("/Checkout");
    mobileBagDialog(false);
  };
  const goToShoppingBag = () => {
    router.push("/shoppingBag");
    mobileBagDialog(false);
  };
  const handleClose = () => {
    mobileBagDialog(false);
    // setOpen(false);
  };

  useEffect(() => {
    if (cartItems.length == 0) {
      handleClose();
    }
  }, [cartItems]);

  const cartItemComp = cartItems.map((item) => {
    return (
      <BagItem
        key={item.id}
        item={item}
        addItemQty={addItemQty}
        removeItemQty={removeItemQty}
        removeFromBag={removeFromBag}
      />
    );
  });

  return (
    <>
      <Dialog
        fullScreen
        open={dialog}
        onClose={handleClose}
        TransitionComponent={Transition}
      >
        <AppBar sx={{ position: "relative", padding: "0px" }}>
          <Toolbar>
            <IconButton
              edge="start"
              color="primary"
              onClick={handleClose}
              aria-label="close"
            >
              <CloseIcon />
            </IconButton>
            <Typography sx={{ ml: 2, flex: 1, color: 'rgba(0,0,0.8)' }} variant="h6" component="div">
              Your Bag
            </Typography>
          </Toolbar>
        </AppBar>
        {cartItems.length > 0 ? (
          <DialogContent sx={{ background: "#eff0f4", paddingBottom: 15 }}>
            {cartItemComp}
          </DialogContent>
        ) : (
          <DialogContent sx={{ background: "#eff0f4", paddingBottom: 15 }}>
            <p className="textCenter">Empty Shopping Bag</p>
          </DialogContent>
        )}
        <Box
          sx={{
            backgroundColor: "#000",
            color: "#fff",
            fontSize: "14px",
            textAlign: "center",
            position: "fixed",
            bottom: 0,
            left: 0,
            width: "100%",
            zIndex: "modal",
            padding: "15px 0px",
            boxShadow: "2px 2px 5px rgba(0,0,0,.5)",
            display: { xs: "block", sm: "none" },
          }}
        >
          <Grid container spacing={2}>
            <Grid item xs={4}>
              Total TK {totalAmount}
            </Grid>
            <Grid item xs={4} onClick={goToShoppingBag}>
              View Bag Details
            </Grid>
            <Grid item xs={4} onClick={goToCheckout}>
              Checkout
            </Grid>
          </Grid>
        </Box>
      </Dialog>
    </>
  );
};

const mapStateToProps = (state) => {
  return {
    cartItems: state.shoppingBag.shoppingCart,
    dialog: state.shoppingBag.mobileBagDialog,
  };
};

function mapDispatchToProps(dispatch) {
  return {
    addItemQty: (product) => dispatch(addItemQty(product)),
    removeItemQty: (product) => dispatch(removeItemQty(product)),
    removeFromBag: (product) => dispatch(removeFromBag(product)),
    mobileBagDialog: (val) => dispatch(mobileBagDialog(val)),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MobileShoppingDialog);
