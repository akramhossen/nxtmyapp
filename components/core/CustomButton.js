import React from 'react'
import Button from '@mui/material/Button'
import { createTheme, ThemeProvider } from '@mui/material/styles'

const theme = createTheme({
  palette: {
    primary: {
      light: 'green',
      main: 'blue',
      dark: 'red',
      contrastText: 'yellow',
    },
  },
})

export default function CustomButton(props) {
  const normal = false
  return (
    <ThemeProvider theme={theme}>
      <div>
        <Button
          variant="contained"
          disableElevation={props.disableElevation?props.disableElevation: normal}
          color="primary"
          fullWidth={props.fullWidth?props.fullWidth: normal}  
        >
          Disable elevation
        </Button>
      </div>
    </ThemeProvider>
  )
}
