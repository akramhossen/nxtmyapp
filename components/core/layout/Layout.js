import React from "react";
import Header from "../../sections/header/Header";
import FooterTop from "../../footer/FooterTop";
import FooterBottom from "../../footer/FooterBottom";
import Container from "@mui/material/Container";
import Box from "@mui/material/Box";
import MobileShoppingDialog from "../../shoppingBag/MobileShoppingDialog";
import SearchPanel from "../../searchpanel/SearchPanel";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import { grey } from "@mui/material/colors";
import MobileAccount from "../../menu/MobileAccount";

const theme = createTheme({
  palette: {
    primary: {
      // Purple and green play nicely together.
      main: grey[900],
    },
    secondary: {
      // This is green.A700 as hex.
      main: "#11cb5f",
    },
  },
});

export default function Layout({ children }) {
  return (
    <div>
      <ThemeProvider theme={theme}>
        {/* <Header /> */}
        <Box sx={{ height: { xs: "61px", sm: "100px" } }} />
        <Container maxWidth="xl">{children}</Container>
        <FooterTop />
        <FooterBottom />
        <MobileShoppingDialog />
        <MobileAccount />
        <SearchPanel />
      </ThemeProvider>
    </div>
  );
}
