import React from "react";

import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";
import { Pagination, Navigation } from "swiper";
import ImageContainer from "../sections/home/image/ImageContainer";

function TopSlider({ deviceWidth }) {
  return (
    <>
      {deviceWidth ? (
        <Swiper
          navigation={true}
          modules={[Pagination, Navigation]}
          className="homeSwiper"
        >
          <SwiperSlide>
            <ImageContainer
              src="/homeAsset/slider/mobile/lubayna.jpg"
              text="Abaya"
              url="/women/abaya"
              priority={ true }
              deviceWidth={deviceWidth}
            />
          </SwiperSlide>
          <SwiperSlide>
            <ImageContainer
              src="/homeAsset/slider/mobile/men.jpg"
              text="all style for men"
              url="/men"
              deviceWidth={deviceWidth}
            />
          </SwiperSlide>
          <SwiperSlide>
            <ImageContainer
              src="/homeAsset/slider/mobile/women.jpg"
              text="all style for women"
              url="/women"
              deviceWidth={deviceWidth}
            />
          </SwiperSlide>
        </Swiper>
      ) : (
        <Swiper
          navigation={true}
          modules={[Pagination, Navigation]}
          className="homeSwiper"
        >
          <SwiperSlide>
            <ImageContainer
              src="/homeAsset/slider/lubayna.jpg"
              text="Abaya"
              url="/women/abaya"
              deviceWidth={deviceWidth}
            />
          </SwiperSlide>
          <SwiperSlide>
            <ImageContainer
              src="/homeAsset/slider/miah-men.jpg"
              text="all style for men"
              url="/men"
              deviceWidth={deviceWidth}
            />
          </SwiperSlide>
          <SwiperSlide>
            <ImageContainer
              src="/homeAsset/slider/miah-women.jpg"
              text="all style for women"
              url="/women"
              deviceWidth={deviceWidth}
            />
          </SwiperSlide>
        </Swiper>
      )}
    </>
  );
}

export default TopSlider;
