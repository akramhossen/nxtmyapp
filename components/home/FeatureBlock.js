import React from "react";
import { Grid, Box } from "@mui/material";
import Image from "next/image";
import { MiahButton } from "../button/MiahButton";
MiahButton;

export default function FeatureBlock({ deviceWidth }) {
  return (
    <Grid container spacing={2}>
      {deviceWidth ? null : (
        <Grid item sm={6} xs={12} sx={{ position: "relative" }}>
          <Box
            sx={{
              position: "absolute",
              top: "50%",
              left: "50%",
              transform: "translate(-50%, -50%)",
              textAlign: "center",
            }}
          >
            <h2 style={{ fontSize: "2rem" }}>Panjabi </h2>
            <p>Buy Like a lord use like a hero</p>
            <MiahButton>Shop Abaya</MiahButton>
          </Box>
        </Grid>
      )}
      <Grid item sm={6} xs={12}>
        <Image
          src="/homeAsset/feature/panjabi-desktop.jpg"
          alt="Miah Banner"
          width={600}
          height={600}
          placeholder="blur"
          blurDataURL="/homeAsset/bckgnd.png"
          layout="responsive"
          objectFit="cover"
        />
      </Grid>
      {deviceWidth ? (
        <Grid item sm={6} xs={12} sx={{ position: "relative" }}>
          <Box
            sx={{
              textAlign: "center",
            }}
          >
            <h3>Panjabi </h3>
            <p>Buy Like a lord use like a hero</p>
            <MiahButton>Shop Abaya</MiahButton>
          </Box>
        </Grid>
      ) : null}
    </Grid>
  );
}
