import React from "react";
import { Box } from "@mui/material";

function Offer() {
  return (
    <Box
      sx={{
        textAlign: "center",
      }}
    >
      <Box
        mb={8}
        sx={{
          textAlign: "center",
          height: '120px'
        }}
        >
        <h3>Offer</h3>
        <video autoPlay loop muted width="100%" height="120px">
          <source type="video/mp4" src="/common-asset/lower-ani2.mp4" />
        </video>
      </Box>
    </Box>
  );
}

export default Offer;
