import React from "react";
// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";

// import required modules
import { Autoplay, Pagination, Navigation } from "swiper";

import Image from "next/image";
import { Box } from "@mui/material";
import Link from "next/link";
const sliders = [
  {
    id: 1,
    scr: "/homeAsset/categories/web-01.png",
    url: "/women/abaya",
    title: "Abaya",
  },
  {
    id: 2,
    scr: "/homeAsset/categories/web-02.png",
    url: "/women/saree",
    title: "Saree",
  },
  {
    id: 3,
    scr: "/homeAsset/categories/web-03.png",
    url: "/men/lungi",
    title: "Lungi",
  },
  {
    id: 4,
    scr: "/homeAsset/categories/web-04.png",
    url: "/women/salwar-kameez",
    title: "Salwar Kameez",
  },
  {
    id: 5,
    scr: "/homeAsset/categories/web-05.png",
    url: "/men/dhoti",
    title: "Dhoti",
  },

  {
    id: 6,
    scr: "/homeAsset/categories/web-06.png",
    url: "/men/panjabi",
    title: "Panjabi",
  },
  {
    id: 7,
    scr: "/homeAsset/categories/web-07.png",
    url: "/men/bottoms/joggers",
    title: "Joggers",
  },
  {
    id: 8,
    scr: "/homeAsset/categories/web-08.png",
    url: "/others/cut-fabrics",
    title: "Cutfabrics",
  },
  {
    id: 9,
    scr: "/homeAsset/categories/web-09.png",
    url: "/others/gamcha",
    title: "Gamcha",
  },
  {
    id: 10,
    scr: "/homeAsset/categories/web-10.png",
    url: "/men/tops",
    title: "Hoodie",
  },
  // {
  //   id: 1,
  //   scr: "/homeAsset/categories/web-11.png",
  //   url: "/men/inner-vest",
  //   title: "Inner Vest",
  // },
  {
    id: 1,
    scr: "/homeAsset/categories/web-12.png",
    url: "/kids/boys/kids-lungi",
    title: "Kids lungi",
  },
];

function CategorySlider({ deviceWidth }) {
  return (
    <Box py={2}>
      <h3 className="textCenter marginBottomZero">Trending Categories</h3>
      <Box py={2} mt={2}>
        <Swiper
          slidesPerView={deviceWidth ? 4 : 6}
          loop={true}
          autoplay={{
            delay: 3000,
            disableOnInteraction: false,
          }}
          centeredSlides={true}
          spaceBetween={deviceWidth ? 2 : 10}
          modules={[Autoplay, Pagination, Navigation]}
          height={520}
          className="centredCategorySlider"
        >
          {sliders.map((slider, pos) => {
            return (
              <SwiperSlide key={pos}>
                <Link href={slider.url}>
                  <a>
                    <Image
                      alt="Mountains"
                      width={400}
                      height={400}
                      src={slider.scr}
                      layout="intrinsic"
                    />
                    <div className="textCenter">
                      <small>{slider.title}</small>
                    </div>
                  </a>
                </Link>
              </SwiperSlide>
            );
          })}
        </Swiper>
      </Box>
    </Box>
  );
}

export default CategorySlider;
