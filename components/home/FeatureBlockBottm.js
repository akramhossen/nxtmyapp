import React from "react";
import { Grid, Box } from "@mui/material";
import Image from "next/image";
import { MiahHomeButtonWhite } from "../button/MiahButton";
import Link from "next/link";

const data = [
  {
    id: 1,
    title: "shop lungi",
    url: "/men/lungi",
    src: "/homeAsset/feature/lungi-desktop.jpg",
  },
  {
    id: 2,
    title: "Shop 3-Pices",
    url: "/women/salwar-kameez",
    src: "/homeAsset/feature/3-pcs-desktop.jpg",
  },
  {
    id: 4,
    title: "Cut Fabrics",
    url: "/others/cut-fabrics",
    src: "/homeAsset/feature/fabrics-desktop.jpg",
  },
  {
    id: 3,
    title: "Shop Sharee",
    url: "/women/saree",
    src: "/homeAsset/feature/sharee-desktop.jpg",
  },
];
export default function FeatureBlockBottm() {
  return (
    <>
      <Grid container spacing={2} pb={12} mb={12}>
        {data.map((dataItem, pos) => {
          return (
            <Grid item sm={6} xs={12} key={pos}>
              <Box sx={{ position: "relative" }}>
                <Image
                  alt="Mountains"
                  width={400}
                  height={400}
                  src={dataItem.src}
                  layout="responsive"
                  placeholder="blur"
                  blurDataURL="/homeAsset/bckgnd.png"
                />
                <Box
                  sx={{
                    position: "absolute",
                    bottom: "10%",
                    left: "50%",
                    transform: "translateX(-50%)",
                  }}
                >
                  <Link href={dataItem.url}>
                    <a>
                      <MiahHomeButtonWhite>{dataItem.title}</MiahHomeButtonWhite>
                    </a>
                  </Link>
                </Box>
              </Box>
            </Grid>
          );
        })}
      </Grid>
    </>
  );
}
