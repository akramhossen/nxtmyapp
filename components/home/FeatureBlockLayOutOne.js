import React from "react";
import { Grid, Box } from "@mui/material";
import Image from "next/image";
import { MiahButton } from "../button/MiahButton";

export default function FeatureBlockLayOutOne({ deviceWidth }) {
  return (
    <Grid container spacing={2}>
      <Grid item sm={6} xs={12}>
        <Image
          src="/homeAsset/feature/abaya-desktop.jpg"
          alt="Miah Banner"
          width={600}
          height={600}
          placeholder="blur"
          blurDataURL="/homeAsset/bckgnd.png"
          layout="responsive"
          objectFit="cover"
        />
      </Grid>
      <Grid item sm={6} xs={12} sx={{ position: "relative" }}>
        {deviceWidth ? (
          <Box
            sx={{
              textAlign: "center",
            }}
          >
            <h3>Modesty Abaya </h3>
            <p>Modesty is the crown of a Muslimah</p>
            <MiahButton>Shop Abaya</MiahButton>
          </Box>
        ) : (
          <Box
            sx={{
              position: "absolute",
              top: "50%",
              left: "50%",
              transform: "translate(-50%, -50%)",
              textAlign: "center",
            }}
          >
            <h2 style={{ fontSize: "2rem" }}>Modesty Abaya </h2>
            <p>Modesty is the crown of a Muslimah</p>
            <MiahButton>Shop Abaya</MiahButton>
          </Box>
        )}
      </Grid>
    </Grid>
  );
}
