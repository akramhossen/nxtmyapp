import React, { useState, useEffect } from "react";
import commonService from "../../service/commonService";
import { Box } from "@mui/material";

// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";

// import required modules
import { Pagination } from "swiper";
import Image from "next/image";
import { IMAGE_URL } from "../../service/serviceConfig";
import Link from "next/link";

function NewArrivals({ deviceWidth, recetView }) {
  // local state
  const [newArriavl, setNewArrival] = useState([]);
  const [bestSells, setBestSells] = useState([]);

  // side effect
  useEffect(() => {
    const getArrivalProduct = () => {
      commonService
        .getData("homeProductList")
        .then((res) => {
          setNewArrival(res.data.newArrivalProducts);
          setBestSells(res.data.bestSellsProducts);
        })
        .catch((err) => console.log(err));
    };
    getArrivalProduct();
  }, []);

  return (
    <>
      {recetView ? (
        <Box
          py={deviceWidth ? 2 : 10}
          my={deviceWidth ? 2 : 10}
          sx={{ textAlign: "center" }}
        >
          {
            recetView.length > 0 ?
            <h3>Recent Viewed Items</h3>
            :
            <h3>Best Sells</h3>
          }

          {recetView.length > 0 ? (
            <Swiper
              pagination={{ clickable: true }}
              slidesPerView={deviceWidth ? 2 : 4}
              spaceBetween={deviceWidth ? 2 : 30}
              modules={[Pagination]}
              className="newArrivalSwiper"
            >
              {recetView.map((item) => {
                return (
                  <SwiperSlide key={item.id}>
                    <Box className="newArraivalProduct">
                      <Link href={"/product/" + item.slug}>
                        <a>
                          <Image
                            alt="Mountains"
                            width={400}
                            height={400}
                            src={IMAGE_URL + item.back_img}
                            layout="intrinsic"
                            placeholder="blur"
                            blurDataURL="/homeAsset/bckgnd.png"
                          />
                          <p className={deviceWidth ? "marginBottomZero" : ""}>
                            {item.name}
                          </p>
                        </a>
                      </Link>
                    </Box>
                  </SwiperSlide>
                );
              })}
            </Swiper>
          ) : (
            <Swiper
              pagination={{ clickable: true }}
              slidesPerView={deviceWidth ? 2 : 4}
              spaceBetween={deviceWidth ? 2 : 30}
              modules={[Pagination]}
              className="newArrivalSwiper"
            >
              {bestSells.map((item) => {
                return (
                  <SwiperSlide key={item.id}>
                    <Box className="newArraivalProduct">
                      <Link href={"/product/" + item.slug}>
                        <a>
                          <Image
                            alt="Mountains"
                            width={400}
                            height={400}
                            src={IMAGE_URL + item.back_img}
                            layout="intrinsic"
                            placeholder="blur"
                            blurDataURL="/homeAsset/bckgnd.png"
                          />
                          <p className={deviceWidth ? "marginBottomZero" : ""}>
                            {item.name}
                          </p>
                        </a>
                      </Link>
                    </Box>
                  </SwiperSlide>
                );
              })}
            </Swiper>
          )}
        </Box>
      ) : (
        <Box
          py={deviceWidth ? 2 : 10}
          my={deviceWidth ? 2 : 10}
          sx={{ textAlign: "center" }}
        >
          <h3>New Arrival</h3>
          <Swiper
            pagination={{ clickable: true }}
            slidesPerView={deviceWidth ? 2 : 4}
            spaceBetween={deviceWidth ? 2 : 30}
            modules={[Pagination]}
            className="newArrivalSwiper"
          >
            {newArriavl.map((item) => {
              return (
                <SwiperSlide key={item.id}>
                  <Box className="newArraivalProduct">
                    <Link href={"/product/" + item.slug}>
                      <a>
                        <Image
                          alt="Mountains"
                          width={400}
                          height={400}
                          src={IMAGE_URL + item.back_img}
                          layout="intrinsic"
                          placeholder="blur"
                          blurDataURL="/homeAsset/bckgnd.png"
                        />
                        <p className={deviceWidth ? "marginBottomZero" : ""}>
                          {item.name}
                        </p>
                      </a>
                    </Link>
                  </Box>
                </SwiperSlide>
              );
            })}
          </Swiper>
        </Box>
      )}
    </>
  );
}

export default NewArrivals;
