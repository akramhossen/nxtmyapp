import React, { Component } from 'react'

class Welcome extends Component {
  render() {
    return (
      <div>
        <h1>Welcome {this.props.name} a.k.a {this.props.heroName}</h1>
        <h1>proj {this.props.prj}</h1>
      </div>
    ) 
    
  }
}

export default Welcome