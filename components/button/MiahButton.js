import React from "react";
import { Button } from "@mui/material";
import { styled } from "@mui/material/styles";
import CircularProgress from "@mui/material/CircularProgress";

const CommonButton = styled(Button)({
  boxShadow: "none !important",
  outline: "none !important",
  border: "1px solid",
  borderRadius: 0,
  borderColor: "#000000",
  background: "rgba(0,0,0,1)",
  color: "#fff",
  "&:hover": {
    backgroundColor: "rgba(0,0,0,0.85)",
  },
});

const HomeWhiteButton = styled(Button)({
  boxShadow: "none !important",
  outline: "none !important",
  border: "1px solid",
  borderRadius: 0,
  borderColor: "#fff",
  background: 'transparent',
  fontSize: '.85rem',
  padding: '0px 5px',
  color: "#fff",
  "&:hover": {
    backgroundColor: "rgba(255,255,255,0.15)",
  },
});


export function MiahButton({ methodFromParent, children }) {
  const handleClick = () => {
    methodFromParent ? methodFromParent() : null;
  };
  return (
    <CommonButton variant="contained" fullWidth onClick={handleClick}>
      {children}
    </CommonButton>
  );
}
export function MiahSubmitButton({ children }) {
  return (
    <CommonButton variant="contained" fullWidth type="submit">
      {children}
    </CommonButton>
  );
}

export function MiahLoadingButton({ children }) {
  return (
    <CommonButton fullWidth>
      {" "}
      <CircularProgress
        color="inherit"
        sx={{ marginRight: "10px" }}
        size={18}
      />{" "}
      {children}
    </CommonButton>
  );
}

export function AddToBagButton({ addItemToBag, children }) {
  const handleClick = () => {
    addItemToBag ? addItemToBag() : null;
  };
  return (
    <CommonButton variant="contained" fullWidth onClick={handleClick}>
      {children}
    </CommonButton>
  );
}
export function MiahButtonRegular({ addItemToBag, children }) {
  const handleClick = () => {
    addItemToBag ? addItemToBag() : null;
  };
  return (
    <CommonButton variant="contained" size="small" onClick={handleClick}>
      {children}
    </CommonButton>
  );
}

// submit button with loading

export function MiahSubmitLoadingButton({ isloading, children, type }) {
  return (
    <Button type={type ? type : ""} variant="contained" size="small">
      {isloading ? <CircularProgress color="inherit" size={18} /> : children}
    </Button>
  );
}

export function MiahHomeButton({ children }) {
  return (
    <CommonButton variant="contained" fullWidth>
      {children}
    </CommonButton>
  );
}
export function MiahHomeButtonWhite({ children }) {
  return (
    <HomeWhiteButton variant="contained" >
      {children}
    </HomeWhiteButton>
  );
}
