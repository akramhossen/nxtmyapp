import React, {useEffect} from "react";
import { Pagination, Box } from "@mui/material";
import { makeStyles } from "@mui/styles";
import { useRouter } from "next/router";

function RootPagination(products) {
  const useStyles = makeStyles({
    root: {
      "& > *": {
        justifyContent: "center",
        display: "flex",
      },
    },
  });
  const classes = useStyles();

  const router = useRouter();
  // =============== state ================
  const [page, setPage] = React.useState(1);

  // =============== methods ==============
  const handleChange = (event, value) => {
    setPage(value);
    if (router.asPath.indexOf("strtprice") !== -1) {
      let prevRoute = router.asPath.split("?");
      router.push(
        prevRoute[0] +
          "?from=" +
          value * 20 +
          "&strtprice=" +
          router.query.strtprice +
          "&endprice=" +
          router.query.endprice
      );
    }

    if (router.asPath.indexOf("strtprice") === -1) {
      let prevRoute = router.asPath.split("?");
      router.push(prevRoute[0] + "?from=" + value * 20);
    }
  };

  // side effect
  useEffect(() => {
    if (router.query.from > 20) {
      setPage(Number(router.query.from ) / 20)
    } else {
      setPage(1)
    }
  }, [router.query]);
  return (
    <>
      <Box
        sx={{
          marginTop: 18,
        }}
      >
        <Pagination
          className={classes.root}
          count={Math.floor(products.products.totalRow / 20)}
          page={page}
          size="small"
          onChange={handleChange}
        />
        <p className="mrY" style={{ textAlign: "center" }}>
          <small>
            {"You've seen "}
            {page * 20} products out of{" "}
            {Math.floor(products.products.totalRow / 20) * 20}
          </small>
        </p>
      </Box>
    </>
  );
}

export default RootPagination;
