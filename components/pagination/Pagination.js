import React from "react";
import { Pagination, Box } from "@mui/material";
import axios from "axios";
import NProgress from "nprogress";
import { makeStyles } from "@mui/styles";
import { BASE_URL } from "../../service/serviceConfig";
function MiahPagination({
  products,
  searchBy,
  parentMethods,
  params,
  priceRange,
}) {
  const useStyles = makeStyles({
    root: {
      "& > *": {
        justifyContent: "center",
        display: "flex",
      },
    },
  });
  const classes = useStyles();
  // =============== state ================
  const [page, setPage] = React.useState(1);
  const [offset, setOffset] = React.useState(1);
  const [count, setCount] = React.useState(2);

  // =============== methods ==============
  const handleChange = (event, value) => {
    setPage(value);
    setOffset(value * 20);
  };

  const getPaginationData = async () => {
    NProgress.start();
    const res = await axios
      .get(
        `${BASE_URL}productByCatSubId?${searchBy}=
          ${params}
          &offset=
          ${offset}
          &attribute=&tags=&price=${priceRange}`
      )
      .then((res) => res);

    if (res.data.status == true) {
      NProgress.done();
      window.scrollTo({
        top: 0,
        behavior: "smooth", // for smoothly scrolling
      });
      parentMethods(res);
    }
  };


  // ================= side effects ==========
  React.useEffect(() => {
    let totalColum = Math.floor(products.totalRow / 20);
    setCount(totalColum);
  }, [products]);

  React.useEffect(() => {
    if (offset > 20) {
      getPaginationData();
    }
  }, [offset]);

  React.useEffect(() => {
    setPage(1);
  }, [priceRange]);

  return (
    <>
      <Box
        sx={{
          marginTop: 18,
        }}
      >
        <Pagination
          className={classes.root}
          count={count}
          page={page}
          size="small"
          onChange={handleChange}
        />
        <p className="mrY" style={{ textAlign: "center" }}>
          <small>
            {"You've seen "}
            {page * 20} products out of {products.totalRow}
          </small>
        </p>
      </Box>
    </>
  );
}

export default MiahPagination;
