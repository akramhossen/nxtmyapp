import React, { useState, useEffect } from "react";
import axios from "axios";
import { useRouter } from "next/router";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import Image from "next/image";
import Dialog from "@mui/material/Dialog";
// import Header from "../../components/sections/header/Header";
import ZoomSlider from "../../components/product/productDetails/ZoomSlider";
import Slide from "@mui/material/Slide";

import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import RelatedProducts from "../../components/product/productDetails/RelatedProducts";
// import FooterTop from "../../components/footer/FooterTop";
// import FooterBottom from "../../components/footer/FooterBottom";

import AddToBagContainer from "../../components/product/productDetails/AddToBagContainer";
import { BASE_URL, IMAGE_URL } from "../../service/serviceConfig";
import MiahBreadCrumbs from "../../components/breadcrumbs/MiahBreadCrumbs";

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export default function Product({ prod, variation, responseData }) {
  // hooks
  const router = useRouter();

  // local state
  const [open, setOpen] = React.useState(false);
  const [zoomImage, setZImage] = React.useState("");
  const [activeIndex, setActiveIndex] = React.useState(0);
  const [clickZoom] = React.useState(true);
  const [products, setProducts] = useState(prod);
  const [prodVari, setprodVari] = useState(variation[0]);
  const [prodVariSize, setprodVariSize] = useState(variation[0].vSize);
  const [slideLength] = useState(prod[0].variants[0].vImage.length);

  // methods
  const handleClose = () => {
    setOpen(false);
  };

  const goToNext = () => {
    if (activeIndex < slideLength - 1) {
      let temp = activeIndex + 1;
      setActiveIndex(temp);
      setZImage(prodVari.vImage[temp].img);
    }
    if (activeIndex === slideLength - 1) {
      setActiveIndex(0);
      setZImage(prodVari.vImage[0].img);
    }
  };
  const seltZoomImage = (image, pos) => {
    setZImage(image);
    setOpen(true);
    setActiveIndex(pos);
  };

  const changeVari = (v) => {
    setprodVari(v);
  };

  const loadStart = () => {
    console.log("start");
  };

  // side effect
  useEffect(() => {
    // setZImage(prod[0].variants[0].vImage[0].img);
    setProducts(prod);
    setprodVari(variation[0]);
  }, [router.asPath, prod, variation]);

  const variComponent = prodVari.vImage.map((image, pos) => {
    return (
      <Grid item md={6} xs={3} key={image.id}>
        <Image
          src={`${IMAGE_URL + "/m_thumb/"}${image.img}`}
          alt={image.img_title}
          width={300}
          height={300}
          layout="responsive"
          placeholder="blur"
          blurDataURL="/homeAsset/bckgnd.png"
          onClick={() => seltZoomImage(image.img, pos)}
        />
      </Grid>
    );
  });

  const variColor = variation.map((vai, pos) => {
    return (
      <Image
        key={pos}
        src={`${IMAGE_URL + "/m_thumb/"}${vai.vImage[0].img}`}
        alt={vai.vImage[0].img_title}
        width={50}
        height={50}
        layout="fixed"
        placeholder="blur"
        blurDataURL="/homeAsset/bckgnd.png"
        onClick={() => changeVari(vai)}
      />
    );
  });
  const relatedProd = products[0].relatedProduct.map((prod) => {
    return (
      <Grid item md={3} xs={6} key={prod.id}>
        <RelatedProducts product={prod} />
      </Grid>
    );
  });
  return (
    <>
      {/* <Header /> */}
      <div className="zoomDialog">
        <Dialog
          fullScreen
          open={open}
          onClose={handleClose}
          TransitionComponent={Transition}
        >
          <ZoomSlider
            clickZoom={clickZoom}
            setZoom={setOpen}
            zoomImage={zoomImage}
            goToNext={goToNext}
          />

          {/* <ProductZoom
            clickZoom={clickZoom}
            setZoom={setOpen}
            zoomImage={zoomImage}
            prodVari={prodVari}
          /> */}
        </Dialog>
      </div>

      <Box sx={{ p: 1, display: { xs: "block", sm: "block" } }}>
        <Grid container spacing={2}>
          <Grid item md={7} xs={12}>
            <MiahBreadCrumbs data={responseData} type="product" />
            {/* <Image
              src={`${IMAGE_URL}${prodVari.vImage[0].img}`}
              alt={prodVari.vImage[0].img_title}
              width={300}
              height={300}
              layout="responsive"
              placeholder="blur"
              blurDataURL="/homeAsset/bckgnd.png"
              property="true"
              onLoad={loadStart}
              onClick={() =>
                seltZoomImage(products[0].variants[0].vImage[0].img, 0)
              }
            /> */}
            <img
              src={`${IMAGE_URL}${prodVari.vImage[0].img}`}
              onClick={() =>
                seltZoomImage(products[0].variants[0].vImage[0].img, 0)
              }
              width="100%"
            />
            <Box sx={{ pt: 2, pb: 2 }}>
              <Grid container spacing={2}>
                {variComponent}
              </Grid>
            </Box>
            <Box sx={{ display: { xs: "block", sm: "none" } }}>
              <p>Colors</p>
              {variColor}
            </Box>
          </Grid>
          <Grid item md={5} xs={12} className="miahfornt">
            <Box sx={{ display: { xs: "none", sm: "block" } }}>
              <h1>{products[0].name}</h1>
              <h2>{products[0].name_bangla}</h2>
            </Box>
            <p>
              <b>{products[0].brand}</b>
            </p>
            <p>Style: {products[0].color}</p>
            <p>Sku: {prodVari.sku}</p>
            <p>
              <b>Tk {products[0].sales_cost}</b>
            </p>
            <Box sx={{ display: { xs: "none", sm: "block" } }}>{variColor}</Box>
            <Grid container spacing={0}>
              <Grid item xs={12}>
                <AddToBagContainer products={products} prodVari={prodVari} />
              </Grid>
              <Grid item xs={12}>
                <p>
                  <strong>Product Description:</strong>
                </p>
                <div
                  style={{ textAlign: "justify" }}
                  className="miahfornt"
                  dangerouslySetInnerHTML={{ __html: products[0].sales_info }}
                />
                <p>
                  <strong>Product Specification:</strong>
                </p>
              </Grid>
              <Grid item xs={12}>
                <TableContainer component={Paper}>
                  <Table
                    sx={{ width: "100%" }}
                    size="small"
                    aria-label="a dense table"
                  >
                    <TableBody>
                      {products[0].attributes.map((row) => (
                        <TableRow
                          key={row.attribute_name}
                          sx={{
                            "&:last-child td, &:last-child th": { border: 0 },
                          }}
                        >
                          <TableCell component="th" scope="row">
                            {row.attribute_name}
                          </TableCell>
                          <TableCell align="right">
                            {row.attribute_value}
                          </TableCell>
                        </TableRow>
                      ))}
                    </TableBody>
                  </Table>
                </TableContainer>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        <Grid container spacing={2}>
          <Grid xs={12} sm={7} item>
            <h2>You May Also Like</h2>
            <hr />
            <Grid container>{relatedProd}</Grid>
          </Grid>
        </Grid>
      </Box>

      {/* <Box>
        <Grid container spacing={1}>
        <Grid item xs={12} sm={12}>
          <Image
            src={`https://images.miah.shop/product/${prodVari.vImage[0].img}`}
            alt="Picture of the author"
            width={300}
            height={300}
            layout="responsive"
            placeholder="blur"
            blurDataURL="/homeAsset/bckgnd.png"
            onClick={() =>
              seltZoomImage(products[0].variants[0].vImage[0].img)
            }
          />
        </Grid>
        </Grid>
      </Box> */}

      {/* <FooterTop />
      <FooterBottom /> */}
    </>
  );
}

export async function getServerSideProps(context) {
  const { params, req, res, query } = context;
  const { product } = params;

  let axiosRes = [];
  axiosRes = await axios
    .get(`${BASE_URL}productById/${product}`)
    .then((response) => {
      if (response.data.data.length > 0) {
        return response.data;
      } else {
        return [];
      }
    })
    .catch((error) => {
      if (error) {
        return [];
      }
      console.log("error print", error);
    });

  return {
    props: {
      prod: axiosRes.data,
      variation: axiosRes.data[0].variants,
      responseData: axiosRes,
    },
  };
}
