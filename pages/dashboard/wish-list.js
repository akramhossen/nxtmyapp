import React from "react";
import WishList from "../../components/dashboard/WishList";
import LayoutDashboard from "../../components/dashboard/LayoutDashboard";

export default function WishListPage() {
  return (
    <>
      <LayoutDashboard>
        <WishList />
      </LayoutDashboard>
    </>
  );
}
