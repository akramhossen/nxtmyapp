import React from "react";
import OrderHistory from "../../components/dashboard/OrderHistory";
import LayoutDashboard from "../../components/dashboard/LayoutDashboard";


export default function OrderHistoryPage() {
  return (
    <>
      <LayoutDashboard>
        <OrderHistory />
      </LayoutDashboard>
    </>
  );
}
