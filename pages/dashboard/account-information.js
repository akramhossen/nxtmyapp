import React from "react";
import AccountInformation from "../../components/dashboard/AccountInformation";
import LayoutDashboard from "../../components/dashboard/LayoutDashboard";

export default function AccountInformationPage() {
  return (
    <>
      <LayoutDashboard>
        <AccountInformation />
      </LayoutDashboard>
    </>
  );
}
