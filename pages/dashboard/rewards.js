import React from "react";
import RewardCashback from '../../components/dashboard/RewardCashback'
import LayoutDashboard from "../../components/dashboard/LayoutDashboard";


export default function Rewards() {
  return (
    <>
      <LayoutDashboard>
        <RewardCashback />
      </LayoutDashboard>
    </>
  );
}
