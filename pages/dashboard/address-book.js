import React from "react";
import AddressBook from "../../components/dashboard/AddresBook";
import LayoutDashboard from "../../components/dashboard/LayoutDashboard";


export default function AddressBookPage() {
  return (
    <>
      <LayoutDashboard>
        <AddressBook />
      </LayoutDashboard>
    </>
  );
}
