import React from "react";
import CancleOrder from "../../components/dashboard/CancleOrder";
import LayoutDashboard from "../../components/dashboard/LayoutDashboard";


export default function CancleOrderPage() {
  return (
    <>
      <LayoutDashboard>
        <CancleOrder />
      </LayoutDashboard>
    </>
  );
}
