import axios from "axios";
import React, { useState, useEffect } from "react";
import { useRouter } from "next/router";
// import Header from '../../../components/sections/header/Header'
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import SingleProduct from "../../../components/product/SingleProduct";
// import PriceRange from "../../../components/filter/PriceRange";
import MiahPagination from "../../../components/pagination/Pagination";
import MiahBreadCrumbs from "../../../components/breadcrumbs/MiahBreadCrumbs";
import { BASE_URL } from "../../../service/serviceConfig";
import RootPriceRange from "../../../components/filter/RootPriceRange";
import RootPagination from "../../../components/pagination/RootPagination";


const SubCategory = ({ data }) => {
  // hooks
  const router = useRouter();

  // locat state
  const [searchBy] = useState("subCategoryId");
  const [params, setParams] = useState(router.query.subCategory);
  const [responseData, setResponseData] = useState(data);
  const [priceRange, setpriceRange] = useState([
    0,
    parseInt(responseData.maxRate),
  ]);

  // methods
  const setPaginationProducts = (res) => {
    setResponseData(res.data.data);
  };

  // use effect
  useEffect(() => {
    setResponseData(data);
    setParams(router.query.subCategory);
  }, [router.asPath]);

  if (router.isFallback) {
    return <div>Loading...</div>;
  }
  return (
    <div>
      {/* <Header/> */}
      <Box sx={{ p: 1 }}>
        <Grid container spacing={0} justifyContent="start">
          <Grid item sm={6} xs={12} mb={2}>
            <MiahBreadCrumbs data={responseData} type="subcategory" />
          </Grid>
        </Grid>
        <Grid container spacing={0} justifyContent="start">
          <Grid item sm={3} xs={12} mb={2}>
            {/* <PriceRange
              params={params}
              searchBy={searchBy}
              parentMethods={setPriceRangeProducts}
              products={responseData}
            /> */}

            <RootPriceRange params={params} products={responseData} />
          </Grid>
        </Grid>
        <Grid container spacing={1}>
          {responseData.product.map((item) => (
            <SingleProduct
              key={item.id}
              product={item}
              variant={item.variant}
            />
          ))}
        </Grid>
        <Grid container spacing={0} justifyContent="center">
          <Grid item sm={3} xs={8} mb={2}>
            {/* <MiahPagination
              products={responseData}
              searchBy={searchBy}
              params={params}
              priceRange={priceRange}
              parentMethods={setPaginationProducts}
            /> */}
             <RootPagination products={responseData} params={params} />
          </Grid>
        </Grid>
      </Box>
    </div>
  );
};
export default SubCategory;

export async function getServerSideProps(context) {
  const { params } = context;

  let offest = 0;
  let priceRange = [];

  if (
    context.req.url.indexOf("from=") !== -1 &&
    context.req.url.indexOf("strtprice=") === -1
  ) {
    let temp = context.req.url.substring(
      context.req.url.indexOf("from=") + 5,
      context.req.url.length
    );

    offest = temp;
  }

  if (
    context.req.url.indexOf("from=") !== -1 &&
    context.req.url.indexOf("strtprice=") !== -1
  ) {
    let temp = context.req.url.substring(
      context.req.url.indexOf("from=") + 5,
      context.req.url.lastIndexOf("&strtprice")
    );

    offest = temp;
  }

  if (context.req.url.indexOf("strtprice=") !== -1) {
    let strtTemp = context.req.url.substring(
      context.req.url.indexOf("strtprice=") + 10,
      context.req.url.lastIndexOf("&endprice")
    );
    let endTemp = context.req.url.substring(
      context.req.url.indexOf("&endprice=") + 10,
      context.req.url.length
    );

    priceRange = [parseInt(strtTemp), parseInt(endTemp)];
  }

  let axiosRes = [];
  axiosRes = await axios
    .get(
      `${BASE_URL}productByCatSubId?subCategoryId=${params.subCategory}&offset=${offest}&attribute=&tags=&price=${priceRange}`
    )
    .then((response) => {
      if (response.data.data.product.length > 0) {
        return response.data.data;
      } else {
        return [];
      }
    })
    .catch((error) => {
      if (error) {
        return [];
      }
      console.log("error print", error);
    });
  if (axiosRes.length < 0) {
    return {
      notFound: true,
    };
  }
  return { props: { data: axiosRes } };
}

// export async function getStaticPaths() {
//   return {
//     paths: [
//         { params: { rootCategory: 'men', category: 'lungi' } },
//         { params: { rootCategory: 'men', category: 'panjabi' } },
//         { params: { rootCategory: 'men', category: 'inner-vest' } },
//         { params: { rootCategory: 'women', category: 'salwar-kameez' } },
//         { params: { rootCategory: 'women', category: 'saree' } },
//       ],
//     fallback: false
//   }
// }
