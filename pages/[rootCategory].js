import axios from "axios";
import React, { useState, useEffect } from "react";
import { useRouter } from "next/router";
import { Grid, Box } from "@mui/material";
import SingleProduct from "../components/product/SingleProduct";
// import PriceRange from "../components/filter/PriceRange";
// import MiahPagination from "../components/pagination/Pagination";
import RootPriceRange from "../components/filter/RootPriceRange";
import MiahBreadCrumbs from "../components/breadcrumbs/MiahBreadCrumbs";
import { BASE_URL } from "../service/serviceConfig";
import RootPagination from "../components/pagination/RootPagination";

const RootCategory = ({ data }) => {
  // hooks
  const router = useRouter();

  // state
  const [params, setParams] = useState(router.query.rootCategory);
  const [responseData, setResponseData] = useState(data);

  // side effects
  useEffect(() => {
    setResponseData(data);
    setParams(router.query.rootCategory);
  }, [router.asPath]);

  if (router.isFallback) {
    return <div>Loading...</div>;
  }
  return (
    <>
      {responseData.length !== 0 ? (
        <Box>
          <Grid container spacing={0} justifyContent="start">
            <Grid item sm={6} xs={12} mb={2}>
              <MiahBreadCrumbs data={responseData} type="rootCategory" />
            </Grid>
          </Grid>
          <Grid container spacing={0} justifyContent="start">
            <Grid item sm={3} xs={12} mb={2}>
              <RootPriceRange params={params} products={responseData} />
            </Grid>
          </Grid>
          <Grid container spacing={1} justifyContent="start">
            {responseData.product.map((item) => (
              <SingleProduct
                key={item.id}
                product={item}
                variant={item.variant}
              />
            ))}
          </Grid>
          <Grid container spacing={0} justifyContent="center">
            <Grid item sm={3} xs={8} mb={2}>
              <RootPagination products={responseData} params={params} />
            </Grid>
          </Grid>
        </Box>
      ) : (
        <Box>
          <Grid container spacing={2} py={12}>
            <Grid item xs={12} sm={12}>
              <p className="textCenter">
                <b>No items avialable</b>
              </p>
            </Grid>
          </Grid>
        </Box>
      )}
    </>
  );
};
export default RootCategory;

export async function getServerSideProps(context) {
  const { params } = context;
  let offest = 0;
  let priceRange = [];

  if (
    context.req.url.indexOf("from=") !== -1 &&
    context.req.url.indexOf("strtprice=") === -1
  ) {
    let temp = context.req.url.substring(
      context.req.url.indexOf("from=") + 5,
      context.req.url.length
    );

    offest = temp;
  }

  if (
    context.req.url.indexOf("from=") !== -1 &&
    context.req.url.indexOf("strtprice=") !== -1
  ) {
    let temp = context.req.url.substring(
      context.req.url.indexOf("from=") + 5,
      context.req.url.lastIndexOf("&strtprice")
    );

    offest = temp;
  }

  if (context.req.url.indexOf("strtprice=") !== -1) {
    let strtTemp = context.req.url.substring(
      context.req.url.indexOf("strtprice=") + 10,
      context.req.url.lastIndexOf("&endprice")
    );
    let endTemp = context.req.url.substring(
      context.req.url.indexOf("&endprice=") + 10,
      context.req.url.length
    );

    priceRange = [parseInt(strtTemp), parseInt(endTemp)];
  }

  let axiosRes = [];
  axiosRes = await axios
    .get(
      `${BASE_URL}productByCatSubId?departmentId=${params.rootCategory}&offset=${offest}&attribute=&tags=&price=${priceRange}`
    )
    .then((response) => {
      if (response.data.data.product.length > 0) {
        return response.data.data;
      } else {
        return [];
      }
    })
    .catch((error) => {
      if (error) {
        return [];
      }
      console.log("error print", error);
    });
  if (axiosRes.length < 0) {
    return {
      notFound: true,
    };
  }
  return { props: { data: axiosRes } };
}

// export async function getStaticPaths() {
//   return {
//     paths: [
//         { params: { rootCategory: 'men' } },
//         { params: { rootCategory: 'women' } },
//         { params: { rootCategory: 'others' } },
//       ],
//     fallback: false
//   }
// }
