import React from "react";
import { Grid, Divider, Box, Button } from "@mui/material";
import RoomIcon from "@mui/icons-material/Room";
import PhoneAndroidOutlinedIcon from "@mui/icons-material/PhoneAndroidOutlined";
import MarkunreadOutlinedIcon from "@mui/icons-material/MarkunreadOutlined";
import TextField from "@mui/material/TextField";
import Link from "next/link";
import MiahButton from "../components/button/MiahButton";

export default function ContactUs() {
  return (
    <div>
      <div className="contactTopArea">
        <h1>CONTACT US</h1>
        <p>
          <Link href="/">
            <a>
              <u>
                <b>Home</b>
              </u>
            </a>
          </Link>
        </p>
      </div>

      <Grid container spacing={0}>
        <Grid item sm={6} xs={12} pt={5}>
          <p>
            <b>KEEP IN TOUCH WITH US</b>
          </p>
          <p>A high-quality clothing and fashion brand by Amanat Shah Group.</p>
          <Grid container spacing={0} mt={5}>
            <Grid item sm={1} xs={2}>
              <RoomIcon />
            </Grid>
            <Grid item sm={5} xs={10}>
              City Centre (Level 24), 90/1 Motijheel, Dhaka, Bangladesh
            </Grid>
            <Grid item sm={12} xs={12} mt={2}>
              <Divider />
            </Grid>
          </Grid>

          <Grid container spacing={0} mt={5}>
            <Grid item sm={1} xs={2}>
              <PhoneAndroidOutlinedIcon />
            </Grid>
            <Grid item sm={5} xs={10}>
              <a href="tel:+8801313767678">+8801313767678</a>
            </Grid>
            <Grid item sm={12} xs={12} mt={2}>
              <Divider />
            </Grid>
          </Grid>

          <Grid container spacing={0} mt={5}>
            <Grid item sm={1} xs={2}>
              <MarkunreadOutlinedIcon />
            </Grid>
            <Grid item sm={5} xs={10}>
              info@miah.shop
            </Grid>
            <Grid item sm={12} xs={12} mt={2}>
              <Divider />
            </Grid>
          </Grid>
        </Grid>
        <Grid item sm={6} xs={12} pt={5}>
          <Box sx={{ padding: { sm: "0px 50px" } }}>
            <Grid container spacing={2}>
              <Grid sm={12} item xs={12}>
                <p>
                  <b>MAIL US YOUR MESSAGE</b>
                </p>
                <TextField fullWidth label="Name" variant="standard" />
              </Grid>
              <Grid sm={12} item xs={12} pt={2}>
                <TextField
                  fullWidth
                  label="Email"
                  type="email"
                  variant="standard"
                />
              </Grid>
              <Grid sm={12} item xs={12} pt={2}>
                <TextField
                  label="Your Need & Description"
                  multiline
                  rows={4}
                  fullWidth
                  variant="standard"
                />
              </Grid>
              <Grid sm={12} item xs={12} >
                <br/>
                <br/>
                <br/>
                <Button variant="contained">Send message</Button>
              </Grid>
            </Grid>
          </Box>
        </Grid>
      </Grid>
    </div>
  );
}
