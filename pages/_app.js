import "../styles/globals.css";
import NProgress from "nprogress";
import "nprogress/nprogress.css";
import Router, { useRouter } from "next/router";
import { useStore } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import Layout from "../components/core/layout/Layout";
import { wrapper } from "../redux/store";
NProgress.configure({
  minimum: 0.7,
  easing: "ease",
  speed: 800,
  showSpinner: false,
});

Router.events.on("routeChangeStart", () => NProgress.start());
Router.events.on("routeChangeComplete", () => NProgress.done());
Router.events.on("routeChangeError", () => NProgress.done());

function MyApp({ session, Component, pageProps }) {
  const store = useStore((state) => state);
  const route = useRouter();
  return (
    <PersistGate persistor={store.__persistor}>
      {/* {route.asPath === "/" ? (
        <Component {...pageProps} />
      ) : (
        <Layout>
          <Component {...pageProps} />
        </Layout>
      )} */}

      {() => (
        <Layout>
          <Component {...pageProps} />
        </Layout>
      )}
    </PersistGate>
  );
}

export default wrapper.withRedux(MyApp);
