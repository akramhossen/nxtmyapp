import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { fetchMenu, setCount } from "../redux/menu/menuActions";
import styles from "../styles/Home.module.css";

import Head from "next/head";
import Header from "../components/sections/header/Header";
import TopSlider from "../components/home/TopSlider";
import CategorySlider from "../components/home/CategorySlider";
import Offer from "../components/home/Offer";
import FeatureBlock from "../components/home/FeatureBlock";
import NewArrivals from "../components/home/NewArrivals";
import FeatureBlockBottm from "../components/home/FeatureBlockBottm";
import { useTheme } from "@emotion/react";
import { useMediaQuery } from "@mui/material";
import FeatureBlockLayOutOne from "../components/home/FeatureBlockLayOutOne";
import Link from "next/link";

export default function Home({menuData}) {
  
  console.log(menuData.data);

  // hooks
  const theme = useTheme();
  const deviceWidth = useMediaQuery(theme.breakpoints.down("sm"));
  const recetView = useSelector((state) => state.auth.recentView);
  const menu = useSelector((state) => state.menu.menu);
  const count = useSelector((state) => state.menu.count);
  const dispatch = useDispatch();

  // methods
  const changeCount = () => {
    dispatch(setCount());
  };

  // side effects

  useEffect(() => {
    dispatch(fetchMenu());
  }, [dispatch]);
  return (
    <React.Fragment>
      <Head>
        <title>
          Miah | Best Online Lungi Saree Three Piece shop in Bangladesh
        </title>
        <meta
          name="description"
          content="Miah Bangladesh's largest traditional online shop. Buy online lungi, panjabi, t-shirt centu genji,  saree, salwar kameez, Cut piece fabrics Ect."
        />
        <meta
          name="keywords"
          content="MIAH, Amanat Shah Group, Amanat Shah Lungi, Amanat Shah Poplin, Amanat Shah Voile, Amanat Shah Gamcha, Standard Lungi, Standard Sharee, Standard Three Piece, Lungi, Sarong, Saree, Three Piece, Cut Fabrics, Online fabrics store, Online shopping, Traditional Fashion Wear, Traditional lifestyle, Lifestyle Brand, Fashion Brand, Bangladeshi Lifestyle, Bangladeshi Fashion, Online Lungi Shop, Online Saree, Online Salwar Kameez"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main>
        <Header />
        <ul>
          {
            menuData.data.map((menu, pos) => {
              return(
              <li key={pos}>
                <Link href={'/' + menu.slug}>
                  <a>{menu.root_category}</a>
                </Link>
              </li>
              )
            })
          }
        </ul>
        {/* <TopSlider deviceWidth={deviceWidth} />
        <CategorySlider deviceWidth={deviceWidth} />
        <Offer />
        <FeatureBlockLayOutOne deviceWidth={deviceWidth} />
        <NewArrivals deviceWidth={deviceWidth} recetView={recetView} />
        <FeatureBlock deviceWidth={deviceWidth} />
        <NewArrivals deviceWidth={deviceWidth} />
        <FeatureBlockBottm /> */}
      </main>
    </React.Fragment>
  );
}


export async function getServerSideProps() {
  const response = await fetch('http://demoapi.miah.shop/api/buildMenu')
  const data = await response.json()
  return {
    props: {
      menuData: data
    }
  }
}