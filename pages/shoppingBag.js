import React, {useEffect} from "react";
import BagItem from "../components/shoppingBag/BagItem";
import CaculateShipping from "../components/shoppingBag/CaculateShipping";
import { connect } from "react-redux";
import { Grid, Box } from "@mui/material";
import { useRouter } from "next/router";
import {
  addItemQty,
  removeItemQty,
  removeFromBag,
  mobileBagDialog,
} from "../redux/shoppingBag/shoppingBagActions";

const shoppingBag = ({
  cartItems,
  addItemQty,
  removeItemQty,
  removeFromBag,
}) => {

  // hooks
  // const route = useRouter()

  // const cartItemComp = cartItems.map((item) => {
  //   return (
  //     <BagItem
  //       key={item.id}
  //       item={item}
  //       addItemQty={addItemQty}
  //       removeItemQty={removeItemQty}
  //       removeFromBag={removeFromBag}
  //     />
  //   );
  // });
  

  // useEffect(() => {
  //   if(cartItems.length == 0) {
  //     route.push('/')
  //   }
  // }, [cartItems])
  
  return (
    <>
      <h1>Your Bag</h1>
      <Grid container>
        <Grid item md={8}>
          {
            cartItems.length > 0
            ?
            (
            <Box sx={{ background: "#eff0f4" }}>
              {cartItems.map((item, pos) => {
                return (
                  <BagItem
                    key={pos}
                    item={item}
                    addItemQty={addItemQty}
                    removeItemQty={removeItemQty}
                    removeFromBag={removeFromBag}
                  />
                );
              })}
            </Box>
            )
            :
            (
              <Box sx={{ background: "#eff0f4", padding: '20px 10px' }}>
                <p className="textCenter">Empty Shopping Bag</p>
              </Box>
            )
          }
        </Grid>
        <Grid item md={4} px={2}>
          <CaculateShipping />
        </Grid>
      </Grid>
    </>
  );
};

const mapStateToProps = (state) => {
  return {
    cartItems: state.shoppingBag.shoppingCart,
    dialog: state.shoppingBag.mobileBagDialog,
  };
};

function mapDispatchToProps(dispatch) {
  return {
    addItemQty: (product) => dispatch(addItemQty(product)),
    removeItemQty: (product) => dispatch(removeItemQty(product)),
    removeFromBag: (product) => dispatch(removeFromBag(product)),
    mobileBagDialog: (val) => dispatch(mobileBagDialog(val)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(shoppingBag);
