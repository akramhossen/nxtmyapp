import React, { useEffect, useState } from "react";
import {Grid, Box} from "@mui/material";
import SingleProduct from "../components/product/SingleProduct";
import commonService from "../service/menu/commonService";
import { useRouter } from "next/router";
import MiahPagination from "../components/pagination/Pagination";

export default function Search() {
  // hooks
  const router = useRouter();

  // local state
  const [responseData, setResponseData] = useState({});
  const [product, setProduct] = useState([]);

  const [searchBy] = useState("search");
  const [params, setParams] = useState(router.query.searchBy);
  const [priceRange, setpriceRange] = useState([
    0,
    parseInt(responseData.maxRate),
  ]);

  // methods

  const setPaginationProducts = (res) => {
    // setResponseData(res.data.data);
    console.log(res.data);
  };


  useEffect(() => {
    if (router.isReady) {
      const getProducts = () => {
        const api =
          "productByCatSubId?search=" +
          router.query.searchBy +
          "&offset=" +
          0 +
          "&attribute=&tags=&price=";
        commonService
          .getData(api)
          .then((res) => {
            setProduct(res.product);
            setResponseData(res);
          })
          .catch((error) => {
            console.log(error);
          });
      };
      
      getProducts();
    }
  }, [router.query, router.isReady]);

  return (
    <Box>
      <Grid container spacing={0} justifyContent="start">
        <Grid sm={12} xs={12} item>
          <h3>Search By : {router.query.searchBy}</h3>
        </Grid>
        {product.map((item) => (
          <SingleProduct key={item.id} product={item} variant={item.variant} />
        ))}
      </Grid>
      {/* <Grid container spacing={0} justifyContent="center">
        <Grid item sm={3} xs={8} mb={2}>
          <MiahPagination
            products={responseData}
            searchBy={searchBy}
            params={params}
            priceRange={priceRange}
            parentMethods={setPaginationProducts}
          />
        </Grid>
      </Grid> */}
    </Box>
  );
}
