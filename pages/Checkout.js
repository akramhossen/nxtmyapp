import React, { useState } from "react";
import { Formik, Form } from "formik";
import * as Yup from "yup";
import { useSelector, useDispatch } from "react-redux";
import {
  fetchLocations,
  setHasShippingAction,
} from "../redux/checkout/checkoutActions";
import {
  Box,
  Checkbox,
  Dialog,
  FormGroup,
  FormControlLabel,
  Grid,
} from "@mui/material";
import Billing from "../components/checkout/Billing";
import Shipping from "../components/checkout/Shipping";
import CheckoutTopArea from "../components/checkout/CheckoutTopArea";
import CardContaier from "../components/checkout/CardContaier";
import { testCheckout } from "../redux/checkout/checkoutActions";
import ReveiwOrder from "../components/checkout/ReveiwOrder";
import OrderSummary from "../components/checkout/OrderSummary";
import PaymentMathod from "../components/checkout/PaymentMathod";
// import { submitOrder } from "../service/order/submitOrder";
import useSchema from "../hooks/useSchema";
import CheckOutSignin from "../components/checkout/CheckOutSignin";
import {
  MiahLoadingButton,
  MiahSubmitButton,
} from "../components/button/MiahButton";
import { useOrderSubmit } from "../hooks/useOrderHooks";
import CheckoutOtpContainer from "../components/opt/CheckoutOtpContainer";
import commonService from "../service/menu/commonService";
import { useRouter } from "next/router";

export default function Checkout() {
  //=================== hooks =================
  const dispatch = useDispatch();
  const route = useRouter();
  const submitOrder = useOrderSubmit();
  const INITIAL_FORM_STATE = useSelector(
    (state) => state.checkout.formInitialValue
  );
  const userInfo = useSelector((state) => state.auth.userInfo);
  const shoppingBag = useSelector((state) =>  state.shoppingBag.shoppingCart);
  const [schema] = useSchema();

  // ============= state =========================
  const [formState, setFormState] = useState(INITIAL_FORM_STATE);
  const [mobileNumberDialog, setMobileNumberDialog] = useState(false);
  const [btnLoading, setBtnLoading] = useState(false);
  const [formValue, setFormValue] = useState({});
  const [otpOpen, setOtpOpen] = useState(false);
  const [otpVarifyBody, setOtpVarifyBody] = useState({});
  const [hasShipping, setHasShipping] = useState(false);
  const [shippingCharge, setShippingCharge] = useState(0);
  const [digitalDiscount, setDigitalDiscount] = useState(0);
  const [couponDiscount, setCouponDiscount] = useState(0);

  // =================== methods =================
  const testFnc = () => {
    dispatch(testCheckout("5"));
  };

  // send otp
  const handleSendOtp = (values) => {
    commonService
      .postData("alienOtp", { phone: values.billigInfo.phone })
      .then((res) => {
        if (res.data.status === true) {
          setOtpOpen(true);
          verifyOtp(values);
        }
        if (res.data.status === false) {
          setMobileNumberDialog(true)
        }
      })
      .catch((error) => {
        console.log("errror", error);
      });
  };

  // verify succedd
  const verifySucceed = () => {
    submitOrder(formValue, userInfo.token);
    setBtnLoading(false);
  };

  // verify otp
  const verifyOtp = (values) => {
    const body = {
      phone: values.billigInfo.phone,
      email: values.billigInfo.email,
      first_name: values.billigInfo.fName,
      last_name: values.billigInfo.lName,
      countryid: 555,
    };
    setOtpVarifyBody(body);
    setFormValue(values);
  };

  // otp dialog
  const handeOtpDialog = (status) => {
    setOtpOpen(status);
  };

  const handleOrderSubmit = (values) => {
    // setBtnLoading(true);
    if (userInfo.token) {
      submitOrder(values, userInfo.token);
    } else {
      handleSendOtp(values);
    }
    // submitOrder(values)
  };

  const handleHasShipping = (event) => {
    dispatch(setHasShippingAction(event.target.checked));
    setHasShipping(event.target.checked);
  };

  const handleShippingCharge = (val) => {
    setShippingCharge(val);
  };

  const handleDigilatDicount = (val) => {
    setDigitalDiscount(val);
  };

  const handleCouponDiscount = (val) => {
    setCouponDiscount(val);
  };

  // ==================== side effects ==============
  // dispatch(fetchLocations());

  React.useEffect(() => {
    if (shoppingBag.length === 0) {
      route.push('/')
    }
  }, [shoppingBag, route]);

  React.useEffect(() => {
    setFormState(INITIAL_FORM_STATE);
  }, [INITIAL_FORM_STATE]);

  React.useEffect(() => {
    dispatch(fetchLocations())
  }, []);
  return (
    <>
      <h1>Checkout</h1>
      <CheckOutSignin />
      <CheckoutTopArea />
      <Formik
        initialValues={{
          ...formState,
        }}
        validationSchema={schema}
        enableReinitialize={true}
        onSubmit={(values) => {
          handleOrderSubmit(values);
        }}
      >
        <Form>
          <Grid container spacing={2}>
            <Grid item sm={6}>
              <CardContaier title="Billing Details">
                <Billing handleShippingCharge={handleShippingCharge} />
                <Box px={2}>
                  <FormGroup>
                    <FormControlLabel
                      control={
                        <Checkbox
                          checked={hasShipping}
                          onChange={handleHasShipping}
                        />
                      }
                      label="Ship to a different address ?"
                    />
                  </FormGroup>
                </Box>
                {hasShipping ? (
                  <Shipping handleShippingCharge={handleShippingCharge} />
                ) : null}
              </CardContaier>
              <br />
              <CardContaier title="Payment method">
                <Box px={2}>
                  <PaymentMathod handleDigilatDicount={handleDigilatDicount} />
                </Box>
              </CardContaier>
            </Grid>
            <Grid item sm={6}>
              <CardContaier title="Reveiw Order">
                <Box px={2}>
                  <ReveiwOrder />
                  <OrderSummary
                    digitalDiscount={digitalDiscount}
                    shippingCharge={shippingCharge}
                    couponDiscount={couponDiscount}
                  />
                </Box>
                <Box py={5} px={2}>
                  {btnLoading ? (
                    <MiahLoadingButton></MiahLoadingButton>
                  ) : (
                    <MiahSubmitButton>Submit</MiahSubmitButton>
                  )}
                </Box>
              </CardContaier>
            </Grid>
          </Grid>
          <br />
        </Form>
      </Formik>
      <button
        style={{ visibility: "hidden" }}
        id="sslczPayBtn"
        token="2345678"
        postdata=""
        order="1"
        endpoint="https://api.miah.shop/api/sslz"
        className="btn btn-primary btn-block"
      >
        Place order
      </button>
      {/* ==================== otp container ==================== */}
      <CheckoutOtpContainer
        otpVarifyBody={otpVarifyBody}
        formValue={formValue}
        open={otpOpen}
        handeDialog={handeOtpDialog}
        verifySucceed={verifySucceed}
      />

      {/* =================== mobile number dialog ============= */}

      <Dialog
        open={mobileNumberDialog}
        onClose={()=>setMobileNumberDialog(false)}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <Box p={5}>
          <p className="textCenter textRed"> Please check your mobile Number </p>
        </Box>
      </Dialog>
      {/* <button onClick={testFnc}>order Submission</button> */}
      {/* <button onClick={testFnc}>Test Button</button> */}
    </>
  );
}
