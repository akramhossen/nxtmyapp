import commonService from "../../service/menu/commonService";
import { TEST_CHEOUT } from "../checkout/checkoutTypes";
import {
  LOG_OUT,
  MOBILE_USER_ACCOUNT,
  SET_AUTH_LOADING,
  SET_RECENT_SEARCH,
  SET_RECENT_VIEW,
  SET_USER_INFO,
} from "./authTypes";

export const setUserInfo = (userInfo) => {
  return {
    type: SET_USER_INFO,
    payload: userInfo,
  };
};

export const fetchUserInfo = (body) => {
  return (dispatch) => {
    dispatch(setAuthLoading(true));
    dispatch(setUserInfo({}));
    commonService
      .postData("login", body)
      .then((res) => {
        dispatch(setAuthLoading(false));
        dispatch(setUserInfo(res.data.data));
        console.log(res.data.data);
      })
      .catch((error) => {
        dispatch(setAuthLoading(false));
        console.log("errror", error);
      });
  };
};

export const setAuthLoading = (loading) => {
  return {
    type: SET_AUTH_LOADING,
    payload: loading,
  };
};

export const testFromAuth = (name) => {
  return {
    type: TEST_CHEOUT,
    payload: name,
  };
};

export const setMobileUserAccount = (value) => {
  return {
    type: MOBILE_USER_ACCOUNT,
    payload: value,
  };
};

export const logout = () => {
  return {
    type: LOG_OUT,
    payload: {},
  };
};

export const setRecentSerach = (item) => {
  return {
    type: SET_RECENT_SEARCH,
    payload: item,
  };
};

export const setRecentView = (item) => {
  return {
    type: SET_RECENT_VIEW,
    payload: item,
  };
};
