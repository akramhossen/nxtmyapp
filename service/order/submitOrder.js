import commonService from "../menu/commonService";

export const submitOrder = (formValues, shoppingBag, userInfo) => {
  let result = 'success'
  const body = { cart: shoppingBag }
    commonService
    .postData("cartStockChk", body)
    .then((res) => {
      if (res.data.status === 0) {
          commonService
          .postAuthData("cashOrder", formValues, userInfo.token)
          .then((res) => {
            console.log(res);
          })
          .catch((error) => {
            console.log(error)
          })
      }
    })
    .catch((error) => {
      console.log(error);
    });
  return result
}
