import axios from "axios"
// const BASE_URL = 'https://api.miah.shop/api/'
// const BASE_URL = 'https://www.miahapi.amanatshahgroup.com/api/'
export const BASE_URL = 'http://demoapi.miah.shop/api/'

export default {
  getData: async function (api) {
    try {
      const response = await axios.get(BASE_URL + api);
      return response.data;
    } catch (error) {
      throw error;
    }
  },
}